import APICALLER from '../config/apiCaller';

class AuthController {
    _loginApi = async (body) => {
        try {
            const endPoint = 'api/login';
            const result = await APICALLER(endPoint, 'POST', body);
            const { data } = result;
            
            if (data.code == 200 && data.success == true) {
                return { err: null, response: data }
            } else {
                return { err: data, response: null }
            }
        } catch(error){
            //console.log(error);
            return { err: true, response: null }
        }
    }

    _registerApi = async (body) => {
        //console.log("body---api login",body);
        try {
            const endPoint = 'api/register';
            const result = await APICALLER(endPoint, 'POST', body);
            const { data } = result;
            //console.log("data---api login",data);
            if (data.code == 200 && data.success == true) {
                return { err: null, response: data }
            } else {
                return { err: data, response: null }
            }
        } catch(error){
                //console.log(error);
                return { err: true, response: null }
        }
    }


}

const AuthPostService = new AuthController();

export default AuthPostService;