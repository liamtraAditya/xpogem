import APICALLER from '../config/apiCaller';

class AppController {
    _GetProductApi = async (body) => {
        console.log('bodyproduct', body);
        try {
            const endPoint = 'api/product_data';
            const result = await APICALLER(endPoint, 'POST', body);
            const { data } = result;
            console.log('data',data);
            if (data.code == 200 && data.success == true) {
                return { err: null, response: data }
            } else {
                return { err: data, response: null }
            }
        } catch(error){
            console.log('error',error);
            return { err: true, response: null }
        }
    }

    _registerApi = async (body) => {
        //console.log("body---api login",body);
        try {
            const endPoint = 'api/register';
            const result = await APICALLER(endPoint, 'POST', body);
            const { data } = result;
            //console.log("data---api login",data);
            if (data.code == 200 && data.success == true) {
                return { err: null, response: data }
            } else {
                return { err: data, response: null }
            }
        } catch(error){
                //console.log(error);
                return { err: true, response: null }
        }
    }


}

const AppPostService = new AppController();

export default AppPostService;