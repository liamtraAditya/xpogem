import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
   flexOne: {
      flex: 1,
   },
   flexFullCenter: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
   },
   flexFullCenterRow: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
   },
   addShadow: {
      shadowColor: "#000",
      shadowOffset: {
         width: 0,
         height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      elevation: 4,
   },

   rowCentered: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
   },
   justifyAlignCenter: {
      justifyContent: 'center',
      alignItems: 'center'
   },
   paddingVertical1x: {
      paddingVertical: hp('1%')
   },
   paddingVertical2x: {
      paddingVertical: hp('2%')
   },
   ImageContainer: {
      width: wp('100%'),
      height: hp('100%'),
   },
   Container: {
      flex: 1,
      backgroundColor: '#fff',
   },
   imgStyle: {
      height: hp('3%'),
      width: hp('4%')
   },
   ImageStyle: {
      width: wp('100%'),
      height: hp('100%'),
   },
   logincontainer: {

      marginTop: hp('35%'),
      height: hp('100%'),
      width: wp('100%'),

   },
   registercontainer: {

      marginTop: hp('5%'),
      height: hp('100%'),
      width: wp('100%'),
      

   },
   forgetcontainer: {

      marginTop: hp('25%'),
      height: hp('100%'),
      width: wp('100%'),

   },
   input: {
      margin: 15,
      height: hp('6.5%'),
      borderColor: '#7a42f4',
      borderBottomWidth: 1,
      color:'#000',
      fontSize: hp('2.5%'),
   },
   passwordContainer: {
      flexDirection: 'row',
      borderBottomWidth: 1,
      borderColor: '#7a42f4',
      height: 40,
      margin: 15,
   },
   inputStyle: {
      width: wp('80%'),
      color:'#000',
      fontSize: hp('2.5%'),
   },
   SearchinputStyle: {
      width: '85%',
      color:'#000',
      fontSize: hp('2.5%'),
   },
   IconStyle: {
      padding: 10,
   },
   submitButton: {
      backgroundColor: '#D2292B',
      height: hp('7%'),
      width: wp('40%'),
      borderRadius: hp('20%')
   },
   floatCenter: {
      alignSelf: 'center',
   },
   floatLeft: {
      alignSelf: 'flex-start',
   },
   floatRight: {
      alignSelf: 'flex-end'
   },
   submitButtonText: {
      color: 'white',
      fontSize: hp('2.5%'),
      paddingTop: hp('1.5%'),

   },
   checkboxContainer: {
      flexDirection: "row",
      padding: hp('1.5%'),
   },
   checkbox: {
      alignSelf: "center",
   },
   label: {
      margin: 8,
   },
   linktextStyle: {
      color: '#D2292B',
      fontSize: hp('2.5%'),
      margin: hp('1.5%'),
      textDecorationLine: "underline",
   },
   textStyle: {
      color: '#000',
      fontSize: hp('2.5%'),
      margin: hp('1%'),
   },
   bottomView: {
      marginTop: hp('20%'),
   },
   regbottomView: {
      marginTop: hp('1%'),
      marginBottom: hp('5%'),
   },
   headcontainer: {
      flexDirection: 'row',
      justifyContent: 'center',
      height: hp('8%')
   },
   backbutton: {
      flex: 0.2,
      justifyContent: 'center',
      alignItems: 'center',

   },
   titletext: {
      flex: 0.6,
      alignItems: 'center',
      justifyContent: 'center'
   },
   headerTitleStyle: {
      color: '#fff',
      fontSize: hp('3%')
   },
   icon: {
      flex: 0.2,
      flexDirection: 'row',
   },
   headerContainer: {
      height: hp('7.8%'),
      backgroundColor: '#26004D',
      flexDirection: 'row',
      justifyContent:'center',
      alignItems:'center'
  },
  footerContainer: {
      height: hp('7.5%'),
      width:wp('100%'),
      backgroundColor: '#fff',
      flexDirection: 'row',
      //justifyContent:'space-evenly',
      justifyContent:'space-evenly',
      alignItems:'center',
      shadowColor: '#000',
      shadowOffset: { width: 1, height: 6 },
      shadowOpacity: 0.8,
      shadowRadius: 2,  
      elevation: 5,
   },
   footericon: {
      height: hp('7%'),
      width: hp('11%'),
      alignSelf:'center',
   },

   //***************Home Banner styles ****************** */
   bannerContainer: {
      height: hp('20%'),
      marginTop: hp('0.5%')
   },
   // *********** category section stylinge starts**************//

   categoryListContainer: {
      flex: 1,
      justifyContent: 'space-around',
      flexDirection: 'row',
      paddingVertical: hp('1%')
   },

   // *************** slide Card style**************//
   slideCardContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: hp('2%'),
      paddingVertical: hp('1%'),
      marginTop:hp('0.5%'),
      backgroundColor:'#fff'
   },
   slideCardHeader: {
      borderBottomColor: '#DBA153',
      borderBottomWidth: hp('0.2%')
   },
   slideCardTitleStyle: {
      fontFamily: 'OpenSans-SemiBold', fontSize: hp('2%'), color: '#351651', fontWeight: 'bold'
   },
   slideCardViewAllTextStyle: {
      color: '#DBA153', fontSize: hp('1.6%'), fontFamily: 'OpenSans-SemiBold'
   },
   slideCardDimensions: {
      height: hp('35%'),
      width: wp('44%'),
      marginHorizontal: hp('1%'),
      borderRadius: hp('0.5%'),
      backgroundColor: '#fff',
      shadowColor: "#000",
      shadowOffset: {
         width: 0,
         height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      elevation: 4,
      marginBottom: hp('1.5%')
   },
   slideCardBottomItemView: {
      paddingHorizontal: hp('1%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'
   },
   slideCardButtomStyle: {
      borderRadius: hp('0.5%'), height: hp('3.5%'), width: hp('15%'), backgroundColor: '#DBA153', justifyContent: 'center', alignItems: 'center'
   },
   slideCardProductNameStyle: {
      fontSize: hp('2%')
   },
   productDetailButtomStyle: {
      flexDirection:'row',
      height: '100%', width: '100%', backgroundColor: '#DBA153', justifyContent: 'center', alignItems: 'center'
   },

// *************** Slide Card Style Ends**************//
//************************ Drawer Style **********//
   DrawerContainer: {
      flex: 1,
      height: hp('100%'),
   },
   DrawerHeader: {
      width:'100%', 
      height: hp('25%'), 
      backgroundColor:'#26024D',
      justifyContent:'center',
      alignSelf:'center',
   },
   DrawerTextStyle: {
      fontFamily: 'OpenSans-SemiBold',
      color: '#fff',
      alignSelf:'center',
   },
   DrawerImageStyle: {
      width:wp('20%'), 
      height: wp('20%'),
      alignSelf:'center',
      borderRadius:wp('20%'),
   },
   DrawerLink:{
      flexDirection:'row',
      width: '95%',
      height:hp('7%'),
      marginTop:hp('2%'),
      paddingLeft:hp('2.5%'),
      borderBottomRightRadius:hp('3%'),
      borderTopRightRadius:hp('3%'),
   },
   DrawerLinkActive:{
      backgroundColor:'#F7F7F7',
   },
   DrawerLinkImage: {
      width:wp('6%'), 
      height: wp('6%'),
      alignSelf:'center',
      marginRight:hp('2%'),
   }, 
//************************ Drawer Style Ends **********//
//************************ Setting Style **********//
SettinglinkContainer:{
         borderWidth:1,
         borderColor:'#F7F7F7',
         backgroundColor:'#fff',
         height:hp('10%'),
         marginTop:hp('2%'),
         margin:hp('0.5%'),
         shadowColor: '#000',
         shadowOffset: {
            width: 5,
            height: 5,
         },
         shadowOpacity: 0.25,
         shadowRadius: 3.84,
         justifyContent:'space-between',
         flexDirection:'row', 
},
settingText:{
 alignSelf:'center',
 marginLeft:hp('5%'),
 fontSize:hp('3.5%'),
},
settingIcon:{
   alignSelf:'center',
   marginRight:hp('5%'),
   height:hp('3%'),
},
settingNotificationIcon:{
   alignSelf:'center',
   marginRight:hp('4%'),
   height:hp('6%'),
   width:wp('16%'),
},

//************************ Setting Style Ends **********//
//************************ Profile Style **********//
profilecontainer: {

   marginTop: hp('2%'),
   height: hp('100%'),
   width: wp('100%'),

},
ProfileImageContainer: {
   width: wp('100%'),
   height: hp('30%'),
   justifyContent:'center',
},
ProfileImageStyle: {
   width:wp('35%'), 
   height: wp('35%'),
   alignSelf:'center',
   justifyContent:'center',
   borderRadius:wp('20%'),
},
profileEditButton: {
   backgroundColor: '#26024D',
   height: hp('5%'),
   width: wp('20%'),
   borderRadius: hp('10%'),
   marginTop:hp('1%'),
},
profileEditButtonText: {
   color: 'white',
   fontSize: hp('2.5%'),
   paddingTop: hp('0.75%'),

},
Proflabeltext: { fontSize: hp('2.5%'), color: '#000',marginLeft: hp('2.5%')},
profilebottomView:{
   flexDirection:'row',
   justifyContent:'center',
},
//************************ Profile Style Ends **********//
Searchbox: {
   flex: 0.9,
   flexDirection:'row',
   borderBottomWidth: 1,
   borderColor: '#F0F0F0',
   backgroundColor: '#F0F0F0',
   borderRadius: hp('10%'),
   height: 40,
   paddingLeft:hp('1%'),
   },
SearchheaderContainer: {
   height: hp('7.8%'),
   backgroundColor: '#fff',
   flexDirection: 'row',
   justifyContent:'center',
   alignItems:'center'
},
searchIconStyle:{
   margin:'11%',
   marginLeft:'1.5%',
   alignSelf:'center',
},
SearchCardHeader: {
   borderBottomColor: '#DBA153',
   borderBottomWidth: hp('0.2%'),
   margin:hp('2%'),
   width:wp('60%'), 
},
//************************ Newsfeed Style Starts **********//



//************************ Newsfeed Style Ends **********//
});

export default styles;
