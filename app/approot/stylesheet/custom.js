import { StyleSheet } from 'react-native';
import { colors } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const styles = StyleSheet.create({
    orderContainer:{
        flex:1
    },
    orderSingleBox:{
        backgroundColor:'#fff',
        padding:10,
        flexDirection:'row',
        flex:1,
        marginBottom:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
    },
    orderDetailSingleBox:{
        backgroundColor:'#fff',
        padding:10,
        flex:1,
        marginBottom:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
    },
     orderContent:{
        flex:0.3,
    },
    orderRightcontent:{
        flex:0.7,
    },
    orderRightShopcontent:{
        flex:0.4,
    },
    orderImage:{
        width:wp('24%'),
        height:hp('12%')
    },
    headingtext:{
        fontSize:14,
        color:'#333',
        marginBottom:2,
    },
    paraText:{
        color:'#999',
        fontSize:12,
    },
    rowdrection:{
        flexDirection:'row',
    },
    sideHead:{
        fontSize:12,
        color:'#333'
    },
    mb3:{
        marginBottom:3
    },
    width100:{
        width:wp('30%'),
    },
    devideOrder:{
        borderTopColor:'#e5e5e5',
        borderTopWidth:1,
        marginTop:4,
        paddingTop:4,
        justifyContent:'space-between'
    },
    BottomButtons:{
        marginTop:1,
        paddingTop:4,
        justifyContent:'space-between',
        flexDirection:'row',
        flex:1
    },
    orderLink:{
        fontSize:12,
        color:'#d7a354'
    },
    orderSliderBox:{
        width:wp('25%'),
        height:hp('13%'),
        marginRight:10,
        marginTop:10,
        borderTopLeftRadius:5,
        borderTopRightRadius:5,
        borderBottomLeftRadius:5,
        borderBottomRightRadius:5,
        marginBottom:20
    },
    productCard: {
        height: hp('33%'),
        width: wp('48%'),
        margin: hp('0.5%'),
        borderRadius: hp('0.5%'),
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
           width: 0,
           height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        
     },
     VendorCard: {
        height: hp('45%'),
        width: wp('48%'),
        margin: hp('0.5%'),
        borderRadius: hp('0.5%'),
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
           width: 0,
           height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        
     },
     ProductSingleBox:{
        flexDirection:'row',
        flex:1,
        paddingTop:3,
        marginBottom:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
    },
     marginTop:{
         marginTop:10
     }
});
export default styles;