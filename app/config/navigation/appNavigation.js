import * as React from 'react';
import homeScreen from '../../view/home/index';
import settingScreen from '../../view/settingsScreens/index';
import privacyScreen from '../../view/settingsScreens/privacypolicy';
import termScreen from '../../view/settingsScreens/terms';
import faqScreen from '../../view/settingsScreens/faq';
import TutScreen from '../../view/splash/tutorial';
import DrawerScreen from '../../view/drawerScreens/index';
import ProfileScreen from '../../view/myprofile/profile';
import SearchScreen from '../../view/searchScreens/searchpage';
import SearchResultScreen from '../../view/searchScreens/searchResultPage';
import ForgetpasswordStepOne from '../../view/myprofile/ChangePasswordStepOne';
import ForgetpasswordStepTwo from '../../view/myprofile/ChangePasswordStepTwo';
import EditProfileScreen from '../../view/myprofile/editprofile';
import { createDrawerNavigator, DrawerContent } from '@react-navigation/drawer';
import OrderScreen from '../../view/orderScreens/order';
import CartScreen from '../../view/orderScreens/cart';
import OrderDetailScreen from '../../view/orderScreens/orderDetail';
import ProductListScreen from '../../view/productScreens/productList';
import ProductDetailScreen from '../../view/productScreens/ProductDetailScreen';
import DesignListScreen from '../../view/designScreens/DesignList';
import DesignDetailScreen from '../../view/designScreens/DesignDetailScreen';
import VendorListScreen from '../../view/showcaseScreens/VendorList';
import ShopDetailScreen from '../../view/showcaseScreens/VendorShopScreen';
import NewsfeedScreen from '../../view/newsfeed/index';

const Drawer = createDrawerNavigator();

function AppStack() {
 
  return (
    <Drawer.Navigator initialRouteName="Home" drawerContent={props => <DrawerScreen {...props} />}>
      <Drawer.Screen name="Home" component={homeScreen}/>
      <Drawer.Screen name="Setting" component={settingScreen}/>
      <Drawer.Screen name="Privacy" component={privacyScreen} />
      <Drawer.Screen name="Terms" component={termScreen} />
      <Drawer.Screen name="Faq" component={faqScreen} />
      <Drawer.Screen name="Search" component={SearchScreen} />
      <Drawer.Screen name="SearchResult" component={SearchResultScreen} />
      <Drawer.Screen name="Tuto" component={TutScreen} />
      <Drawer.Screen name="Profile" component={ProfileScreen} />
      <Drawer.Screen name="Order" component={OrderScreen} />
      <Drawer.Screen name="Cart" component={CartScreen} />
      <Drawer.Screen name='OrderDetail' component={OrderDetailScreen} />
      <Drawer.Screen name='ProductList' component={ProductListScreen} />
      <Drawer.Screen name='ProductDetail' component={ProductDetailScreen} />
      <Drawer.Screen name='DesignList' component={DesignListScreen} />
      <Drawer.Screen name='DesignDetail' component={DesignDetailScreen} />
      <Drawer.Screen name='VendorList' component={VendorListScreen} />
      <Drawer.Screen name='ShopDetail' component={ShopDetailScreen} />
      <Drawer.Screen name='Newsfeed' component={NewsfeedScreen} />
      <Drawer.Screen name="EditProfile" component={EditProfileScreen} />
      <Drawer.Screen name="ChangepasswordOne" component={ForgetpasswordStepOne} />
      <Drawer.Screen name="ChangepasswordTwo" component={ForgetpasswordStepTwo} />
    </Drawer.Navigator>
  );
}


export default AppStack;