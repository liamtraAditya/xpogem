import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../../view/authScreens/login';
import SignupScreen from '../../view/authScreens/signup';
import ForgetpasswordStepOne from '../../view/authScreens/ForgotPasswordStepOne';
import ForgetpasswordStepTwo from '../../view/authScreens/ForgotPasswordStepTwo';
const Stack = createStackNavigator();
function AuthStack() {
  return (
    <Stack.Navigator initialRouteName="Login" headerMode="none">
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="Signup" component={SignupScreen} />
      <Stack.Screen name="ForgetpasswordOne" component={ForgetpasswordStepOne} />
      <Stack.Screen name="ForgetpasswordTwo" component={ForgetpasswordStepTwo} />
    </Stack.Navigator>
  );
}

export default AuthStack;