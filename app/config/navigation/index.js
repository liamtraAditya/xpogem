import React, { useState, useEffect } from 'react';
import AuthNavigation from './authNavigation';
import AppNavigation from './appNavigation'
import SplashScreen from '../../view/splash/index';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {connect} from 'react-redux';
import {setUserData} from '../../config/redux/actions/user';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createStackNavigator();
const RootNavigation = (props) => {
  
  const [isLoading, setIsLoading] = useState(true);
  const [isUser, setIsUser] = useState(false);
  

  const _initialLoadFunction = async () => {
    return new Promise( async (resolve)=>{
      await AsyncStorage.getItem('userData').then(response => {
        const userData = JSON.parse(response);
        
        if (userData) {
          console.log('user data=>>>>', userData);
          props.dispatch(setUserData(userData));
          setIsUser(true);
          resolve(true)
        }
        else {
          setIsUser(false);
          resolve(true)

        }
  
      });
    })
    
  };

  useEffect(() => {
    async function loadfuncion(){
      setTimeout( async () => {
        await _initialLoadFunction();
        setIsLoading(false);
      }, 3000);
    }
    loadfuncion();
  }, [])

  

  if (isLoading) {

    return <SplashScreen />

  } else {
 
    return (
      
      <NavigationContainer>
          
          {isUser?<Stack.Navigator headerMode="none" initialRouteName='App'><Stack.Screen name="App" component={AppNavigation} options={{
            headerShown: false
          }} /><Stack.Screen name="Auth" component={AuthNavigation} options={{
            headerShown: false
          }} /></Stack.Navigator>:<Stack.Navigator headerMode="none" initialRouteName='Auth'><Stack.Screen name="App" component={AppNavigation} options={{
            headerShown: false
          }} /><Stack.Screen name="Auth" component={AuthNavigation} options={{
            headerShown: false
          }} /></Stack.Navigator>
          }
          
        
      </NavigationContainer>

    )

  }
}

export default connect()(RootNavigation);