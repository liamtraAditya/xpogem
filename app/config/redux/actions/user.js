import ActionType from '../constants/index';

export const setUserData = param => {
  return {
    type: ActionType.SET_USER_DATA,
    payload: param,
  };
};

export const setUserLoggedOut = param => {
  return {
    type: ActionType.SET_USER_LOGGEDOUT,
    payload: {},
  };
};
