import ActionType from '../constants/index';
const initialState = {
  user: {},
  isUserLoggedIn: false,
};

const UserReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.SET_USER_DATA:
      // console.log('Services', state);
      return Object.assign({}, state, {
        user: action.payload,
      });

    case ActionType.SET_USER_LOGGEDOUT:
      return Object.assign({}, state, {
        user: {},
      });

    default:
      return state;
  }
};

export default UserReducer;
