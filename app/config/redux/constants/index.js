import keyMirror from 'fbjs/lib/keyMirror';

const ActionTypes = keyMirror({
  SET_USER_DATA: 'SET_USER_DATA',
  SET_USER_LOGGEDOUT:'SET_USER_LOGGEDOUT'
});

export default ActionTypes;
