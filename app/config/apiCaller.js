import axios from 'axios';

// const mode = 'development';
// const [hostName, sethostName] = 'http://xpogem.developerteam.in/';


const APICaller = (endpoint, method, body) =>
  axios({
    url: 'http://xpogem.developerteam.in/' + endpoint,
    method: method || 'GET',
    data: body,
    headers: {
      Accept: 'application/json, text/plain,text/html */*',
      'Content-Type': 'application/json',
    },
    responseType: 'json',
  })
    .then(response => {
      console.log(`response from ${endpoint} >> ${response}`);
      return response;
    })
    .catch(error => {
      console.log(`Error from ${endpoint} >> ${error}`);
      throw error.response;
    });


export default APICaller;
