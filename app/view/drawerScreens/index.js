import React from 'react';
import ProfImg from '../../approot/images/profile-img.png';
import HomeImg from '../../approot/images/sidebar/home.png';
import UserImg from '../../approot/images/sidebar/user.png';
import OrdrImg from '../../approot/images/sidebar/my-order.png';
import SettingImg from '../../approot/images/sidebar/setting.png';
import TutImg from '../../approot/images/sidebar/tutorial.png';
import LogoutImg from '../../approot/images/sidebar/logout.png';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import {setUserLoggedOut} from '../../config/redux/actions/user';
import CustomStyle from '../../approot/stylesheet/index';
import AsyncStorage from '@react-native-async-storage/async-storage';

const DrawerComponent = (props) => {
    //console.log('sdsadsa',props.user.userData);
   const activelink = props.state.index;
   const activelinkname = props.state.routes[activelink].name;
   const source = props.user.userData && props.user.userData.profile_img !== null
        ? { uri: props.user.userData.img_url }
        : ProfImg;
    
    return (
    <View style={CustomStyle.DrawerContainer}>
        <View style={CustomStyle.DrawerHeader}>
            <Image source={source} style={CustomStyle.DrawerImageStyle} />
            <Text style={[CustomStyle.DrawerTextStyle, { fontSize: hp('2.5%') }]}>
                {props.user.userData && props.user.userData.first_name!=null?props.user.userData.first_name+' '+props.user.userData.last_name:'Guest'}
            </Text>
            <Text style={CustomStyle.DrawerTextStyle}>
                {props.user.userData &&  props.user.userData.email!=null?props.user.userData.email:'Guest@XpoGem.com'}
            </Text>
        </View>
        <View style={CustomStyle.DrawerBody}>

            <TouchableOpacity style={[CustomStyle.DrawerLink, activelinkname == "Home" ? CustomStyle.DrawerLinkActive:null]}

             onPress={() => props.navigation.navigate('Home')}>
                <Image source={HomeImg} style={CustomStyle.DrawerLinkImage} />
                <Text style={{ fontSize: hp('2.5%'), color: '#000', alignSelf: 'center', }}>Home</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[CustomStyle.DrawerLink, activelinkname == "Profile" ? CustomStyle.DrawerLinkActive:null]} onPress={() => props.navigation.navigate('Profile')}>
                <Image source={UserImg} style={CustomStyle.DrawerLinkImage} />
                <Text style={{ fontSize: hp('2.5%'), color: '#000', alignSelf: 'center', }}>My Profile</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[CustomStyle.DrawerLink, activelinkname == "Order" ? CustomStyle.DrawerLinkActive:null]} onPress={() => props.navigation.navigate('Order')}>
                <Image source={OrdrImg} style={CustomStyle.DrawerLinkImage} />
                <Text style={{ fontSize: hp('2.5%'), color: '#000', alignSelf: 'center', }}>My Orders</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[CustomStyle.DrawerLink, activelinkname == "Setting" ? CustomStyle.DrawerLinkActive:null]} 
            onPress={() => props.navigation.navigate('Setting')} >
                <Image source={SettingImg} style={CustomStyle.DrawerLinkImage} />
                <Text style={{ fontSize: hp('2.5%'), color: '#000', alignSelf: 'center', }}>Settings</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[CustomStyle.DrawerLink, activelinkname == "Tuto" ? CustomStyle.DrawerLinkActive:null]} onPress={() => props.navigation.navigate('Tuto')}>
                <Image source={TutImg} style={CustomStyle.DrawerLinkImage} />
                <Text style={{ fontSize: hp('2.5%'), color: '#000', alignSelf: 'center', }}>Tutorials</Text>
            </TouchableOpacity>
            <TouchableOpacity style={CustomStyle.DrawerLink} 
            onPress={async () => {
                await AsyncStorage.removeItem('userData');
                // props.dispatch(setUserLoggedOut());
                props.navigation.navigate('Auth');
              }}>
                <Image source={LogoutImg} style={CustomStyle.DrawerLinkImage} />
                <Text style={{ fontSize: hp('2.5%'), color: '#000', alignSelf: 'center', }}>Logout</Text>
            </TouchableOpacity>

        </View>
    </View>
        );
    }
 
    const mapStateToProps = state => ({
        user: state.UserReducer.user,
      });
    export default connect(mapStateToProps)(DrawerComponent);
