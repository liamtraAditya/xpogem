import React from 'react';
import {ImageBackground, Image, View, TouchableOpacity,FlatList, TextInput, Text, ScrollView} from 'react-native';
import Style from '../../approot/stylesheet/custom';
import HomeScreenHeader from '../reusableComponents/homeScreenHeader';
import CustomStyle from '../../approot/stylesheet/index';
import vendorImage from '../../approot/images/vendor-img.png';
import SliderComponent from '../reusableComponents/slider'
import SliderImage1 from '../../approot/images/banner-home-1.png';
import SliderImage2 from '../../approot/images/banner-home-2.png';
import SliderImage3 from '../../approot/images/sliderImage.png';
import SliderImage4 from '../../approot/images/sliderImage2.jpeg';
import FontAwsome from 'react-native-vector-icons/FontAwesome';
const sliderArray = [SliderImage1, SliderImage2, SliderImage3, SliderImage4]

class ProductDetailScreen extends React.Component 
{
     
  render() {

     return (
      <>
        <HomeScreenHeader title="Product Detail" navigation={this.props.navigation}/>
            <ScrollView>
                <SliderComponent slideImage={sliderArray} /> 
                <View style = {Style.orderContainer}>
                    <View style={Style.orderDetailSingleBox}>
                        <View style={[Style.rowdrection]}>
                            <View style={Style.orderRightcontent}>
                                <Text style={Style.headingtext}>Product Name Here..</Text>
                                <View style={[Style.rowdrection]}>
                                    <Text style={Style.sideHead}>Category :- </Text>
                                    <Text style={Style.paraText}>GEM STONE{'\n'}</Text>
                                </View>
                                <View style={[Style.rowdrection]}>
                                    <FontAwsome name="star" color='#DBA153' size={25}/>
                                    <Text style={{ fontSize:16,color:'#999',marginLeft:10}}>3.4{'\n'}</Text>
                                </View>
                            
                            </View>
                            <View style={[Style.orderContent]}>
                                <FontAwsome name="heart-o" color='#DBA153' size={25} style={{alignSelf:'flex-end'}}/>
                            </View>
                        </View>
                        
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>Available Quantity : </Text>
                            <Text style={[Style.paraText]}>30</Text>
                        </View>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>Product Price : </Text>
                            <Text style={[Style.paraText]}>$ 560.90</Text>
                        </View>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>SKU Code :  </Text>
                            <Text style={[Style.paraText]}>Lorem ipsum dolor sit amet,</Text>
                        </View>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>Product ID :  </Text>
                            <Text style={[Style.paraText]}>JS665F8e97</Text>
                        </View>                   
                    </View>
                    <View style={Style.orderDetailSingleBox}>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead]}>Product Description :  </Text>
                        </View>   
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.paraText]}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod b
                            ibendum laoreet. Proin gravida dolor sit amet lacus ac cumsan et viverra ju
                            sto commodo
                            </Text>
                        </View>
                        <Text style={{color: '#D2292B',textDecorationLine: "underline", fontSize:12, alignSelf:'flex-end',}}>
                            Read more...
                        </Text>
                    </View>
                    <View style={Style.orderSingleBox}>
                        <View style={Style.orderContent}>
                            <Image source={vendorImage} style={Style.orderImage}></Image>
                        </View>
                        <View style={Style.orderRightcontent}>
                            <Text style={Style.headingtext} onPress={() => this.props.navigation.navigate('ShopDetail')}>Vendor Name Here..</Text>
                            {/* <Text style={[Style.paraText, Style.mb3]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text> */}
                            <View style={[Style.rowdrection]}>
                                <Text style={[Style.sideHead]}>Store Name :- </Text>
                                <Text style={[Style.paraText]}>GEM STONE{'\n'}</Text>
                            </View>
                            <Text style={{color: '#D2292B',textDecorationLine: "underline",}} onPress={() => this.props.navigation.navigate('ShopDetail')}>Visit Vendor Shop</Text>
                        </View>
                        
                    </View>
                </View>
            </ScrollView> 
            <TouchableOpacity style={CustomStyle.footerContainer} onPress={() => this.props.navigation.navigate('Cart')}>       
                <View style={CustomStyle.productDetailButtomStyle}>
                    <FontAwsome name="cart-plus" size={25} color="#fff" />
                    <Text style={{ fontSize: 20, color: '#fff' }}> Add To Cart</Text>
                </View>
            </TouchableOpacity> 
       </>

    );
  }
}
export default ProductDetailScreen;
