import React from 'react';
import {ImageBackground, Image, View,TouchableOpacity,FlatList, TextInput, Text, ScrollView} from 'react-native';
import Style from '../../approot/stylesheet/custom';
import HomeScreenHeader from '../reusableComponents/homeScreenHeader';
import ScreenFooter from '../reusableComponents/internalScreenFooter';
import orderImage from '../../approot/images/order.png';


class OrderScreen extends React.Component 
{
     
  render() {
//    console.log('test',this.props);
     return (
      <>
        <HomeScreenHeader title="My Order" navigation={this.props.navigation}/>
            <ScrollView> 
                <View style = {Style.orderContainer}>
                <FlatList style={{backgroundColor:'#fff'}} showsHorizontalScrollIndicator={false} horizontal={false} data="hello00" renderItem={() => {
                return <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderDetail')}>
             <View style={Style.orderSingleBox}>
                <View style={Style.orderContent}>
                    <Image source={orderImage} style={Style.orderImage}></Image>
                </View>
                <View style={Style.orderRightcontent}>
                    <Text style={Style.headingtext}>Product Name Here..</Text>
                    <Text style={[Style.paraText, Style.mb3]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>
                    <View style={[Style.rowdrection]}>
                        <Text style={[Style.sideHead]}>Status :- </Text>
                        <Text style={[Style.paraText]}>Delivered @ 13 july 2020</Text>
                    </View>
                    <View style={[Style.rowdrection]}>
                        <Text style={[Style.sideHead]}>Order ID :- </Text>
                        <Text style={[Style.paraText]}>JS7676HHa</Text>
                    </View>
                    <View style={[Style.rowdrection]}>
                        <Text style={[Style.sideHead]}>Order Date :- </Text>
                        <Text style={[Style.paraText]}>12/12/2020</Text>
                    </View>
                    <View style={[Style.devideOrder,Style.rowdrection,Style.flexOne ]}>
                        <Text style={[Style.sideHead]}>Vendor Detail :- </Text>
                        
                    </View>
                    <View>
                        <Text style={[Style.paraText]}>Vendor Name</Text>
                        <Text style={[Style.paraText]}>vendoremail@gmail.com</Text>
                        <Text style={[Style.paraText]}>1234567890</Text>
                    </View>
                </View>
            </View></TouchableOpacity>
            }} />
                </View>
            </ScrollView> 
       <ScreenFooter navigation={this.props.navigation}/>  
       </>

    );
  }
}
export default OrderScreen;
