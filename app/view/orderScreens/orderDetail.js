import React from 'react';
import {ImageBackground, Image, View,TouchableOpacity,FlatList, TextInput, Text, ScrollView} from 'react-native';
import Style from '../../approot/stylesheet/custom';
import HomeScreenHeader from '../reusableComponents/homeScreenHeader';
import ScreenFooter from '../reusableComponents/internalScreenFooter';
import orderImage from '../../approot/images/order.png';


class OrderDetailScreen extends React.Component 
{
     
  render() {
//    console.log('test',this.props);
     return (
      <>
        <HomeScreenHeader title="Order Detail" navigation={this.props.navigation}/>
            <ScrollView> 
                <View style = {Style.orderContainer}>
                    <View style={Style.orderDetailSingleBox}>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>Order ID : </Text>
                            <Text style={[Style.paraText]}>PD1234567890 </Text>
                        </View>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>Order Date : </Text>
                            <Text style={[Style.paraText]}>2/12/2020</Text>
                        </View>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>Order Total : </Text>
                            <Text style={[Style.paraText]}>$ 560.90</Text>
                        </View>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>Product Name :  </Text>
                            <Text style={[Style.paraText]}>Lorem ipsum dolor sit amet,</Text>
                        </View>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>Product ID :  </Text>
                            <Text style={[Style.paraText]}>JS665F8e97</Text>
                        </View>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead]}>Product Description :  </Text>
                        </View>   
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.paraText]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod b
                            ibendum laoreet. Proin gravida dolor sit amet lacus ac cumsan et viverra ju
                            sto commodo</Text>
                        </View>                   
                    </View>
                    <View style={Style.orderDetailSingleBox}>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead]}>Images :</Text>
                        </View>
                        <View>
                        <FlatList style={{backgroundColor:'#fff'}} showsHorizontalScrollIndicator={false} horizontal={true} data="hello00" renderItem={() => {
                            return  <View style={Style.orderSliderBox}>
                                        <Image style={Style.orderSliderBox} source={orderImage}></Image>
                                    </View>
                            }} />
                        </View>
                    </View>
                    <View style={Style.orderDetailSingleBox}>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead]}>Address Details :  </Text>
                        </View>   
                        <View style={[Style.mb3]}>
                            <Text style={[Style.paraText]}>Rosalyn Williams,</Text>
                            <Text style={[Style.paraText]}>Restfield White City London G12 Ariel Way,</Text>
                            <Text style={[Style.paraText]}>United Kingdom</Text>
                        </View> 
                    </View>
                    <View style={Style.orderDetailSingleBox}>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead]}>Payment Details :  </Text>
                        </View>   
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>Order ID : </Text>
                            <Text style={[Style.paraText]}>PD1234567890 </Text>
                        </View>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>Order Date : </Text>
                            <Text style={[Style.paraText]}>2/12/2020</Text>
                        </View>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>Order Total : </Text>
                            <Text style={[Style.paraText]}>$ 560.90</Text>
                        </View>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>Product Name :  </Text>
                            <Text style={[Style.paraText]}>Lorem ipsum dolor sit amet,</Text>
                        </View>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead, Style.width100]}>Product ID :  </Text>
                            <Text style={[Style.paraText]}>JS665F8e97</Text>
                        </View>                   
                    </View>
                </View>
            </ScrollView> 
       <ScreenFooter navigation={this.props.navigation}/>  
       </>

    );
  }
}
export default OrderDetailScreen;
