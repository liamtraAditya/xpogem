import React from 'react';
import {ImageBackground, Image, View,TouchableOpacity,FlatList, TextInput, Text, ScrollView} from 'react-native';
import Style from '../../approot/stylesheet/custom';
import HomeScreenHeader from '../reusableComponents/homeScreenHeader';
import CustomStyle from '../../approot/stylesheet/index';
import orderImage from '../../approot/images/order.png';
import FontAwsome from 'react-native-vector-icons/FontAwesome';
import NumericInput from 'react-native-numeric-input';

class CartScreen extends React.Component 
{
     
  render() {
//    console.log('test',this.props);
     return (
      <>
        <HomeScreenHeader title="My Cart" navigation={this.props.navigation}/>
            <ScrollView> 
                <View style = {Style.orderContainer}>
                <FlatList style={{backgroundColor:'#fff'}} showsHorizontalScrollIndicator={false} horizontal={false} data="hello00" renderItem={() => {
                return <View style={Style.orderSingleBox}>
                <View style={Style.orderContent}>
                    <Image source={orderImage} style={Style.orderImage}></Image>
                </View>
                <View style={Style.orderRightcontent}>
                    <Text style={Style.headingtext} onPress={() => this.props.navigation.navigate('ProductDetail')}>Product Name Here..</Text>
                    <Text style={[Style.paraText, Style.mb3]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. consectetur adipiscing elit.</Text>
                    <View style={[Style.rowdrection]}>
                        <Text style={[Style.sideHead]}>Product ID :- </Text>
                        <Text style={[Style.paraText]}>JS7676HHa</Text>
                    </View>
                    <View style={[Style.rowdrection]}>
                        <FontAwsome name="star" color='#DBA153' size={25}/>
                        <Text style={{ fontSize:16,color:'#999',marginLeft:10}}>3.4</Text>
                    </View>
                    <View style={[Style.devideOrder,Style.rowdrection,Style.flexOne ]}>
                        <View style={[Style.rowdrection,Style.flexOne ]}>
                            <Text style={[Style.headingtext]}>Quantity : </Text>
                            <NumericInput 
                                onLimitReached={(isMax,msg) => console.log(isMax,msg)}
                                totalWidth={100} 
                                totalHeight={30} 
                                iconSize={30}
                                step={1}
                                initValue={1}
                                minValue={1}
                                valueType='integer'
                                
                                textColor='#B0228C' 
                                iconStyle={{ color: 'white' }} 
                                rightButtonBackgroundColor='#26004D' 
                                leftButtonBackgroundColor='#26004D'/>
                        </View>
                        <Text><FontAwsome name="trash" color='#D2292B' size={25}/></Text>
                    </View>
                </View>
            </View>
            }} />
                </View>
            </ScrollView> 
            <View style={CustomStyle.footerContainer}>       
                <View style={CustomStyle.productDetailButtomStyle}>
                    <FontAwsome name="cart-plus" size={25} color="#fff" />
                    <Text style={{ fontSize: 20, color: '#fff' }}> Checkout</Text>
                </View>
            </View>  
       </>

    );
  }
}
export default CartScreen;
