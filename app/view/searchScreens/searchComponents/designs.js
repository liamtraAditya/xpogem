import React from 'react';
import {View, ScrollView} from 'react-native';
import SliderCard from '../../reusableComponents/DesignsliderCard';

const DesignScreens = (prosliprops) => {
    
     return <>
            <ScrollView style={{backgroundColor:'#fff'}}> 
                <View style={{flex:1,justifyContent:'center',marginTop:'2.5%', marginLeft:'2.5%'}} >
                    <SliderCard searchTerm="Best Selling Products" featured='0' colNumber='2' horizontalView={false} navigation={prosliprops.navigation}/>
                </View>
            </ScrollView> 
            </>
}
export default DesignScreens;
