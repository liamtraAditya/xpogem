import {Component} from 'react';
import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions, 
} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SearchScreenHeader from '../reusableComponents/SearchHeader';
import Designers from './searchComponents/designs';
import Products from './searchComponents/products';
import Vendors from './searchComponents/vendors';

const {width, height} = Dimensions.get('window');

class SearchResult extends Component {
  state = {
    index: 0,
    routes: [
      {key: 'Products', title: 'Products'},
      {key: 'Designers', title: 'Designs'},
      {key: 'Vendors', title: 'Vendors'},
    ],
  };


  render() {

    return (
    <>
      <SearchScreenHeader navigation={this.props.navigation}/>
      <View style={styles.mainContainer} showsHorizontalScrollIndicator={true}>
        <TabView
          // abc={this.props.navigation}
          navigationState={this.state}
          renderScene={SceneMap({
            Products: () => (
              <Products navigation={this.props.navigation}/>
            ),
            Designers: () => (
              <Designers navigation={this.props.navigation}/>
            ),
            Vendors: () => (
              <Vendors navigation={this.props.navigation}/>
            ),
           
          })}
          renderTabBar={props => (
            <TabBar
              {...props}
              
              renderLabel={({route}) => (
                <Text style={styles.tabtext}>{route.title}</Text>
              )}
              getLabelText={({route: {title}}) => title}
              style={styles.tab}
              activeColor="#20294F"
              inactiveColor="#666666"
              indicatorStyle={{
                backgroundColor: '#20294F',
                borderBottomWidth: hp('0.25%'),
                scrollEnabled: true,
              }}
            />
          )}
          onIndexChange={index => this.setState({index})}
          initialLayout={{width: Dimensions.get('window').width}}
        />

        
      </View>
    </>  
    );
  }
}

const styles = StyleSheet.create({
  screenTitle: {
    fontFamily: 'Poppins-SemiBold',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: hp('3.8%'),
  },
  tabtext: {
    fontSize: hp('2.5%'),
    fontFamily: 'OpenSans-Bold',
  },
  actionButtonIcon: {
    fontSize: hp('2.5%'),
    height: hp('2.75%'),
    color: '#20294f',
  },

  tab: {
    backgroundColor: '#fff',
    paddingVertical: hp('1%'),
    elevation: 1,
  },
  mainContainer: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },

  mainScrollView: {
    flex: 1,
    paddingBottom: 0.1 * width,
  },
  fiilterContainer: {
    height: hp('35%'),
    marginTop: hp('1.8%'),
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: -4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 5,
  },
  fiilterInnerContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 0.01 * width,
  },
  flexOneWithCenter: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default SearchResult;
