import React from 'react';
import { ImageBackground, Image, View, TouchableOpacity, TextInput, Text, ScrollView, FlatList } from 'react-native';
import CustomStyle from '../../approot/stylesheet/index';
import SearchScreenHeader from '../reusableComponents/SearchHeader';
import ScreenFooter from '../reusableComponents/internalScreenFooter';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SliderImage3 from '../../approot/images/sliderImage.png';
import SliderImage4 from '../../approot/images/sliderImage2.jpeg';
import CategoryButton from '../reusableComponents/categoryButton';

class SearchScreen extends React.Component {

    render() {
        return (
            <>
                <SearchScreenHeader navigation={this.props.navigation}/>
                <ScrollView style={{backgroundColor:'#fff',}}>
                   
                <View style={[CustomStyle.flexOne, CustomStyle.addShadow, { marginTop: hp('0.5%'), backgroundColor: '#fff' }]}>
                        <View style={CustomStyle.SearchCardHeader}>
                            <Text style={[CustomStyle.slideCardTitleStyle,{fontSize: hp('3.5%')}]}>Search By Category</Text>
                        </View> 
                        <View style={CustomStyle.categoryListContainer}>

                            <CategoryButton name="Stone Decor" image={SliderImage3} navigation={this.props.navigation}/>
                            <CategoryButton name="Rocks" image={SliderImage4} navigation={this.props.navigation}/>
                            <CategoryButton name="Minirals" image={SliderImage3} navigation={this.props.navigation}/>
                            <CategoryButton name="Loose Gems" image={SliderImage4} navigation={this.props.navigation}/>

                        </View>
                        <View style={CustomStyle.categoryListContainer}>
                            <CategoryButton name="Jwellery" image={SliderImage3} navigation={this.props.navigation}/>
                            <CategoryButton name="Minirals" image={SliderImage4} navigation={this.props.navigation}/>
                            <CategoryButton name="GEMs" image={SliderImage3} navigation={this.props.navigation}/>
                            <CategoryButton name="Loose" image={SliderImage4} navigation={this.props.navigation}/>
                        </View>
                    </View>
                </ScrollView>
                <ScreenFooter title="Terms & Conditions" navigation={this.props.navigation}/>  
            </>
        );
    }

}
export default SearchScreen;