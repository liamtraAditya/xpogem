import React from 'react';
import CustomStyle from '../../approot/stylesheet/index';
import backImage from '../../approot/images/back-arrow.png';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';


const HomeScreenHeader = (props) => {
    //console.log(props);
    return <>
      <View style={CustomStyle.headerContainer}>
           <TouchableOpacity
               style = {CustomStyle.backbutton}
               onPress = {()=>props.navigation.goBack()}
            >
                  <Image source = {backImage} style={CustomStyle.imgStyle} />
            </TouchableOpacity>
        <View style={{ flex: 0.6 ,justifyContent:'center',alignItems:'center'}}>
            <Text style={{
                fontSize:hp('3%'),
                color:'#fff',
                fontFamily:'OpenSans-SemiBold'
            }}>{props.title}</Text>
        </View>
        <View style={{ flex: 0.2, flexDirection:'row', justifyContent:'space-between', marginRight:hp('3%') }}>
            {/* <TouchableOpacity onPress = {() => this.updateState} style={{height:'100%',}} >
                <Icon name="search" size={25} color="#fff"  />
            </TouchableOpacity>    
            
            <Icon name="cart-plus" size={25} color="#fff" />
            <Icon name="bell-o" size={25} color="#fff" /> */}
        </View>

        
    </View>
    </>
}

export default HomeScreenHeader