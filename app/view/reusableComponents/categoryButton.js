import React from 'react';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { TouchableOpacity, Text, Image, StyleSheet } from 'react-native';

const CategoryButton = (props) => {
    return <TouchableOpacity style={styles.categoryButtonContainer} onPress={()=> props.navigation.navigate('ProductList')}>
        <Image source={props.image} style={styles.categoryImageStyle} />
        <Text style={styles.categoryNameTextStyle}>{props.name}</Text>
    </TouchableOpacity>
}

const styles = StyleSheet.create({
    categoryButtonContainer: {
        flex: .25, justifyContent: 'center', alignItems: 'center'
    },
    categoryImageStyle: {
        height: hp('7%'),
        width: hp('7%'),
        borderRadius: hp('7%') / 2
    },
    categoryNameTextStyle: {
        paddingTop: hp('1%'),
        fontFamily: 'OpenSans-SemiBold',
        color: '#250D46'
    }
})

export default CategoryButton