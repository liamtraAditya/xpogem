import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import CustomStyle from '../../approot/stylesheet/index';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';


const HomeScreenHeader = (props) => {
    //console.log(props);
    return <>
      <View style={CustomStyle.headerContainer}>
        <View style={{ flex: 0.2, marginLeft:hp('3%') }}>
        <TouchableOpacity onPress = {() => props.navigation.openDrawer()}>
              <Icon name="navicon" size={25} color="#fff" />
        </TouchableOpacity>
           
        </View>
        <View style={{ flex: 0.5 ,justifyContent:'center',alignItems:'center'}}>
            <Text style={{
                fontSize:hp('3%'),
                color:'#fff',
                fontFamily:'OpenSans-SemiBold'
            }}>{props.title}</Text>
        </View>
        <View style={{ flex: 0.3, flexDirection:'row', justifyContent:'space-between', marginRight:hp('3%') }}>
            <TouchableOpacity onPress = {() => props.navigation.navigate('Search')} style={{height:'100%',}} >
                <Icon name="search" size={25} color="#fff"  />
            </TouchableOpacity>    
            <TouchableOpacity onPress = {() => props.navigation.navigate('Cart')} style={{height:'100%',}} >
                <Icon name="cart-plus" size={25} color="#fff" />
            </TouchableOpacity>
            
            <Icon name="bell-o" size={25} color="#fff" />
        </View>

        {/* <Image source={props.image} style={styles.categoryImageStyle} />
        <Text style={styles.categoryNameTextStyle}>{props.name}</Text> */}
    </View>
    </>
}

export default HomeScreenHeader