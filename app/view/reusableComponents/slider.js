import React from 'react';
import { View, Text, Image } from 'react-native';
import Swiper from 'react-native-swiper';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const SliderComponent = (props) => {
    return <View style={{ height: hp('25%'), width: wp('100%') }}>
        <Swiper style={{
            height: hp('25%'),
             }} 
            showsButtons={false}
            autoplay={true}
        >
            {props.slideImage.map(item => {

                return <View ><Image style={{
                    height: hp('25%'),
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }} source={item} resizeMode='cover' /></View>
            })}
        </Swiper>
    </View>

}

export default SliderComponent