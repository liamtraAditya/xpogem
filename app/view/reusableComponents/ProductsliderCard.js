import React, { useState, useEffect } from 'react';
import { TouchableOpacity, View, Text, FlatList, Image } from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FontAwsome from 'react-native-vector-icons/FontAwesome';
import SliderImage1 from '../../approot/images/sliderImage.png';
import Styling from '../../approot/stylesheet/index';
import AppServiceCaller from '../../controllers/appController';

const SlideCard = (sliprops) => {
    const [isProduct, setIsProduct] = useState('');
    const _handleProductApi = async () => {
        const body = sliprops.searchTerm;
        const productData = await AppServiceCaller._GetProductApi(body);
        console.log('productDataresponsedata', sliprops.searchTerm);
        setIsProduct(productData.response.productData);
    
      }

    _handleProductApi();


    return <>
        
        <View >
            <FlatList style={{backgroundColor:'#fff'}} numColumns={sliprops.colNumber} showsHorizontalScrollIndicator={sliprops.horizontalView} horizontal={sliprops.horizontalView} data={isProduct} renderItem={({item, index,}) => {
                //console.log("is product---",item,"i-----",index);
                return <View style={Styling.slideCardDimensions}>
                    <TouchableOpacity style={{ height: '65%', width: '100%' }} onPress = {() => sliprops.navigation.navigate('ProductDetail')}>
                       <Image source={SliderImage1} style={{ height: '100%', width: '100%' }} />
                    </TouchableOpacity>
                    
                    <View style={[Styling.flexOne, { backgroundColor: '#fff' }]}>
                        <View style={Styling.slideCardBottomItemView}>
                            <Text style={Styling.slideCardProductNameStyle} onPress = {() => sliprops.navigation.navigate('ProductDetail')}>
                                {item.product_title}
                            </Text>
                            <FontAwsome name="heart-o" color='#DBA153' size={20}/>
                        </View>

                        <View style={{ paddingHorizontal: hp('1%'), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                            <Text style={{ color: '#DBA153', fontSize: hp('2.5%') }}>${item.selling_price}</Text>
                            <Text style={{ color: '#999', fontSize: hp('1.8%'), marginLeft: hp('0.2%') }}>${item.product_price}</Text>
                        </View>

                        <View style={Styling.slideCardBottomItemView}>
                            <View style={Styling.rowCentered}>
                                <FontAwsome name="star" color='#DBA153' />
                                <Text style={{ fontSize: hp('1.5%'), fontFamily: 'OpenSans-SemiBold' }}>3.4</Text>
                            </View>

                            <TouchableOpacity style={Styling.slideCardButtomStyle} onPress = {() => sliprops.navigation.navigate('Cart')}>
                                <Text style={{ fontSize: hp('2.5%'), color: '#fff' }}>Add To Cart</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            }} />
        </View>
    </>
}

export default SlideCard