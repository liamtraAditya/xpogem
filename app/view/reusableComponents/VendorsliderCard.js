import React from 'react';
import { TouchableOpacity, View, Text, FlatList, Image } from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FontAwsome from 'react-native-vector-icons/FontAwesome'
import SliderImage1 from '../../approot/images/member1.png';
import Styling from '../../approot/stylesheet/index';
import customSheet from '../../approot/stylesheet/custom';

const SlideCard = (sliprops) => {
      //console.log('slidecard',sliprops);
    return <>
           <View >
            <FlatList style={{backgroundColor:'#fff'}} numColumns={sliprops.colNumber} showsHorizontalScrollIndicator={sliprops.horizontalView} horizontal={sliprops.horizontalView} data="hello00" renderItem={() => {
                return <View style={Styling.slideCardDimensions}>
               
                    <TouchableOpacity style={{ height: '56%', width: '100%' }} onPress={() => sliprops.navigation.navigate('ShopDetail')}>
                        <Image source={SliderImage1} style={{ height: '100%', width: '100%', alignSelf:'baseline' }} />
                    </TouchableOpacity>
                   
                    <View style={[Styling.flexOne, { backgroundColor: '#fff' }]}>
                        <View style={[Styling.slideCardBottomItemView, Styling.marginTop,{flexDirection:'column'}]}>
                            <Text style={Styling.slideCardProductNameStyle} onPress={() => sliprops.navigation.navigate('ShopDetail')}>Vendor Name</Text>
                            <Text style={[Styling.slideCardProductNameStyle,{color:'#DBA153'}]} onPress={() => sliprops.navigation.navigate('ShopDetail')}>Vendor Shop Name</Text>
                        </View>

                        

                        <View style={Styling.slideCardBottomItemView}>
                            <View style={Styling.rowCentered}>
                                <FontAwsome name="star" color='#DBA153' />
                                <Text style={{ fontSize:12,color:'#999',marginRight:'65%',margin:5}}>3.4</Text>
                                <FontAwsome name="heart-o" color='#DBA153' size={20}/>
                            </View>
                        </View>
                       
                            <View style={[Styling.slideCardButtomStyle,{alignSelf:'center'}]}>
                                <Text style={{ fontSize: hp('1.7%'), color: '#fff', alignSelf:'center' }} onPress={() => sliprops.navigation.navigate('ShopDetail')}>Visit Shop</Text>
                            </View>
                       
                            
                        
                    </View>
                </View>
               
          
            }} />
        </View>
    </>
}

export default SlideCard