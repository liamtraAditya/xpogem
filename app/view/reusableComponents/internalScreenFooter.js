import React from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import BuyIcon from '../../approot/images/sidebar/buy.png';
import ShowIcon from '../../approot/images/sidebar/showcase.png';
import DesignIcon from '../../approot/images/sidebar/design.png';
import NewsIcon from '../../approot/images/sidebar/newsfeed.png';
import CustomStyle from '../../approot/stylesheet/index';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';



const ScreenFooter = (props) => {
    
    return <>
        <View style={[CustomStyle.footerContainer,{}]}>
            <TouchableOpacity onPress = {() => props.navigation.navigate('ProductList')} style={{flexDirection:'column',}}>
                <Icon name="shopping-cart" color='#26004D' size={25} style={{alignSelf:'center'}}/>
                <Text style={{alignSelf:'center'}}>Buy</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress = {() => props.navigation.navigate('VendorList')} style={{flexDirection:'column',}}>
                <Icon name="shopping-bag" color='#26004D' size={25} style={{alignSelf:'center'}}/>
                <Text style={{alignSelf:'center'}}>Showcase</Text>
            </TouchableOpacity>    
            <TouchableOpacity onPress = {() => props.navigation.navigate('DesignList')} style={{flexDirection:'column',}}> 
                <Icon name="diamond" color='#26004D' size={25} style={{alignSelf:'center'}}/>
                <Text style={{alignSelf:'center'}}>Designs</Text>
            </TouchableOpacity>  
            <TouchableOpacity onPress = {() => props.navigation.navigate('Newsfeed')} style={{flexDirection:'column',}}>
                <Icon name="rss" color='#26004D' size={25} style={{alignSelf:'center'}}/>
                <Text style={{alignSelf:'center'}}>Newsfeed</Text>
            </TouchableOpacity>  
        </View>

    </>
}

export default ScreenFooter;