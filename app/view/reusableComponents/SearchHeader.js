import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import CustomStyle from '../../approot/stylesheet/index';
import backImage from '../../approot/images/back-arrow-2.png';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { View, Text, Image, TextInput, StyleSheet, TouchableOpacity } from 'react-native';


const SearchHeader = (props) => {
    console.log(props);
    return <>
       <View style={CustomStyle.SearchheaderContainer}>
            <TouchableOpacity style = {[CustomStyle.backbutton,{ flex: 0.1,}]} onPress = {()=>props.navigation.goBack()} >
                    <Image source = {backImage} style={CustomStyle.imgStyle} />
            </TouchableOpacity>
            <View style={CustomStyle.Searchbox}>
                <TextInput
                    style={CustomStyle.SearchinputStyle}
                        autoCorrect={false}
                        placeholderTextColor = "#000"
                        placeholder="Search for products, vendors or designs"
                />
                <TouchableOpacity onPress = {()=>props.navigation.navigate('SearchResult')} >
                    <Icon
                        style={CustomStyle.searchIconStyle}
                        name='search'
                        color='#000'
                        size={18}
                    />
                </TouchableOpacity>
                
            </View>
        </View>
    </>
}

export default SearchHeader