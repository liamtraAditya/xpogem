import React from 'react';
import {ImageBackground, Image, View,TouchableOpacity, TextInput, Text, ScrollView, Alert} from 'react-native';
import HomeScreenHeader from '../reusableComponents/internalScreenHeader';
import CustomStyle from '../../approot/stylesheet/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import CheckBox from '@react-native-community/checkbox';
import AuthServiceCaller from '../../controllers/authController';

class SignupScreen extends React.Component{

   constructor(props) {
      super(props);
      this.state = {
        FirstName: '',
        lastname:'',
        email: '',
        phone: '',
        password: '',
        cnfrmPassword:'',
        errMessage: '',
        termCondition: false,
        isLoad: false,
        secureTextEntry:true,
      };
    }

    _validateFields = () => {
      if (this.state.FirstName === '' && this.state.lastname === '' && this.state.email === '' && this.state.phone === '' && this.state.password === '' && this.state.cnfrmPassword === '') 
      {
        this.setState({errMessage: 'Please fill all fields'});
        return false;
      } else if (this.state.password != this.state.cnfrmPassword) {
        this.setState({errMessage: "Password & Confirm Password doesn't match"});
        return false;
      } else if (this.state.termCondition == false) {
         this.setState({errMessage: 'Please accept Terms & Condition'});
         return false;
       }else {
        return true;
      }
    };

    _handleRegisterApi = async () => {
      this.setState({isLoad: true});
      const body = {
        first_name: this.state.FirstName,
        last_name:this.state.lastname,
        email: this.state.email,
        phone: this.state.phone,
        password: this.state.password,
        password_confirmation:this.state.cnfrmPassword,
        termCondition:this.state.termCondition,
     };
     const status = await this._validateFields();
     if (status) {
        
      const userdata = await AuthServiceCaller._registerApi(body);
         if(userdata.err){
            this.setState({isLoad: false});
            Alert.alert('Error', userdata.err.message);
         }
         else{
            
            Alert.alert(
               'Success',
               userdata.response.message,
               [
                 {
                   text: 'Login',
                   onPress: () => this.props.navigation.navigate('Login')
                 },
                 
               ],
               { cancelable: false }
             );
           
            
         }


      } else {
         this.setState({isLoad: false});
         Alert.alert('Error', this.state.errMessage);
      }
      //console.log(body);

   }
   
  
  render() {
   //console.log(this.state);
     return (
      <ScrollView style={{backgroundColor:'#fff',}}>
      <HomeScreenHeader title="Register Yourself" navigation={this.props.navigation}/>
          
         <ScrollView style = {CustomStyle.registercontainer}>
          
         <TextInput style = {CustomStyle.input}
              underlineColorAndroid = "transparent"
              placeholder = "First Name"
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              onChangeText={FirstName => this.setState({FirstName})}
         />

         <TextInput style = {CustomStyle.input}
              underlineColorAndroid = "transparent"
              placeholder = "Last Name"
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              onChangeText={lastname => this.setState({lastname})}
         />
           
         <TextInput style = {CustomStyle.input}
              underlineColorAndroid = "transparent"
              placeholder = "Email ID"
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              onChangeText={email => this.setState({email})}
              />

           <TextInput style = {CustomStyle.input}
              underlineColorAndroid = "transparent"
              placeholder = "Mobile No."
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              onChangeText={phone => this.setState({phone})}
              /> 
           
           <View style={CustomStyle.passwordContainer}>
               <TextInput
                  style={CustomStyle.inputStyle}
                     autoCorrect={false}
                     secureTextEntry = {this.state.secureTextEntry}
                     placeholderTextColor = "#000"
                     placeholder="Password"
                     onChangeText={password => this.setState({password})}
                  />
               <TouchableOpacity onPress={()=>this.setState({secureTextEntry: !this.state.secureTextEntry})}> 
                        <Icon                        
                           style={CustomStyle.IconStyle}
                           name='eye'
                           color='#000'
                           size={20}
                        />
               </TouchableOpacity>
            </View>
            
               <TextInput
                  style={CustomStyle.input}
                     autoCorrect={false}
                     secureTextEntry = {true}
                     placeholderTextColor = "#000"
                     placeholder="Confirm Password"
                     onChangeText={cnfrmPassword => this.setState({cnfrmPassword})}
                  />
              

            
            <View style={CustomStyle.checkboxContainer}>
               <CheckBox
                  disabled={false}
                  style={CustomStyle.checkbox}
                  onValueChange={()=>this.setState({termCondition:!this.state.termCondition})}
               />
               <Text style={CustomStyle.textStyle}>I Accept Terms & Conditions</Text>
            </View>
               
            
  
           <TouchableOpacity
              style = {[CustomStyle.submitButton, CustomStyle.floatCenter]}
              onPress={() => this._handleRegisterApi()}
              >
                
                <Text style = {[CustomStyle.submitButtonText, CustomStyle.floatCenter]}> Register Now </Text>
                
           </TouchableOpacity>
           <View style ={CustomStyle.regbottomView}>
              <Text style = {[CustomStyle.textStyle, CustomStyle.floatCenter]}>Already have an account? <Text onPress = {()=>this.props.navigation.navigate('Login')} style = {[CustomStyle.linktextStyle, CustomStyle.floatCenter]}>Sign In</Text></Text>
           </View>
          
        </ScrollView>
       
       
      </ScrollView>
    );
  }
}

export default SignupScreen;