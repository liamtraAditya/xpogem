import React from 'react';
import { ImageBackground, View, TouchableOpacity, TextInput, Text, ScrollView, Alert} from 'react-native';
import splashImage from '../../approot/images/login-screen.jpg';
import CustomStyle from '../../approot/stylesheet/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import AuthServiceCaller from '../../controllers/authController';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {connect} from 'react-redux';
import {setUserData} from '../../config/redux/actions/user';


class LoginScreen extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
        email: '',
        password: '',
        errMessage: '',
        isLoad: false,
        secureTextEntry:true,
      };
    }

    _validateFields = () => {
      if (this.state.email === '') {
        this.setState({errMessage: 'Please fill the email or mobile number'});
        return false;
      } else if (this.state.password === '') {
        this.setState({errMessage: 'Please fill the password'});
        return false;
      } else {
        return true;
      }
    };

    _handleLoginApi = async () => {
    this.setState({isLoad: true});
    const body = {
      email: this.state.email,
      password: this.state.password,
      device_token: 'fL_J2oskAn4:APA91bHvQEdszslOV4tf7ZNaK7uEmjO9xlbe56l1cfIkJCHpO9HNVA5dsfwsuhhU7Be1KjU5D1bGYiocV3qNHJoqAd6w8SMudFfdD5rOw9Bl-Euk0Tjpun22OZCMMtXFvvBB-Xv1B88H',
    };
    const status = await this._validateFields();
    if (status) {
      const userdata = await AuthServiceCaller._loginApi(body);
      console.log('responsedata', userdata);
      if(userdata.err){
         this.setState({isLoad: false});
         Alert.alert('Error', userdata.err.message);
      }
      else{
         
         await AsyncStorage.setItem(
            'userData',
            JSON.stringify(userdata.response),
          );
          this.props.dispatch(setUserData(userdata.response));
          this.props.navigation.navigate('App');
      }
    } else {
      this.setState({isLoad: false});
      Alert.alert('Error', this.state.errMessage);
    }
  };

   render() {
      //console.log('props',this.props);
      return (
         <ScrollView>
            <ImageBackground
               style={CustomStyle.ImageContainer}
               imageStyle={CustomStyle.ImageStyle}
               source={splashImage}>
               <View style={CustomStyle.logincontainer}>

                  <TextInput style={CustomStyle.input}
                     underlineColorAndroid="transparent"
                     placeholder="Email ID / Mobile Number"
                     placeholderTextColor="#000"
                     autoCapitalize="none"
                     onChangeText={email => this.setState({email})}
                  />

                  <View style={CustomStyle.passwordContainer}>
                     <TextInput
                        style={CustomStyle.inputStyle}
                        autoCorrect={false}
                        secureTextEntry={this.state.secureTextEntry}
                        placeholderTextColor="#000"
                        placeholder="Password"
                        onChangeText={password => this.setState({password})}
                     />
                     <TouchableOpacity onPress={()=>this.setState({secureTextEntry: !this.state.secureTextEntry})}> 
                        <Icon                        
                           style={CustomStyle.IconStyle}
                           name='eye'
                           color='#000'
                           size={20}
                        />
                     </TouchableOpacity>
                    
                  </View>

                  <Text style={[CustomStyle.linktextStyle, CustomStyle.floatRight]} onPress={() => this.props.navigation.navigate('ForgetpasswordOne')}>Forget Password?</Text>
                  <TouchableOpacity
                     style={[CustomStyle.submitButton, CustomStyle.floatCenter]}
                     onPress={() => this._handleLoginApi()}
                  >

                     <Text style={[CustomStyle.submitButtonText, CustomStyle.floatCenter]}> Sign In Now </Text>
                  </TouchableOpacity>
                  <View style={CustomStyle.bottomView}>
                     <Text style={[CustomStyle.textStyle, CustomStyle.floatCenter]}>Don't have an account? <Text onPress={() => this.props.navigation.navigate('Signup')} style={[CustomStyle.linktextStyle, CustomStyle.floatCenter]}>Register Now</Text></Text>
                  </View>

               </View>
            </ImageBackground>
         </ScrollView>

      )
   }
}


export default connect()(LoginScreen)
;
