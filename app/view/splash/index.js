import React from 'react';
import {ImageBackground,View,Text,Image} from 'react-native';
import splashImage from '../../approot/images/splash-screen.png';
import CustomStyle from '../../approot/stylesheet/index';
import LogoImage from '../../approot/images/splash-logo.png';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

class SplashScreen extends React.Component {

 
  render() {
    return (
      <ImageBackground
       style={CustomStyle.ImageContainer}
         imageStyle={{resizeMode: 'stretch', width: '100%',height:'100%',justifyContent:'center',flex:1}}
        source={splashImage}>
          <Image source={LogoImage} style={{ alignSelf:'center',marginTop:'30%'}}/>
      </ImageBackground>
    );
  }
}

export default SplashScreen;
