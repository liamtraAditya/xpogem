import React from 'react';
import { ImageBackground, Image, View, TouchableOpacity, TextInput, Text, ScrollView, FlatList } from 'react-native';
import CustomStyle from '../../approot/stylesheet/index';
import Swiper from 'react-native-swiper';
import HomeScreenHeader from '../reusableComponents/internalScreenHeader';
import slide1 from '../../approot/images/tutorial.jpg';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class TutScreen extends React.Component {

    render() {
        return (
            <>
                <HomeScreenHeader title="Tutorials" navigation={this.props.navigation}/>
                <View style={{ height: hp('100%'), width: wp('100%') }}>
                    <Swiper style={{height: hp('100%'),}} showsButtons={true} autoplay={false}>
                         <View ><Image style={{
                                height: hp('100%'),
                                width: '100%',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }} source={slide1} resizeMode='cover' /></View>
                            <View ><Image style={{
                                height: hp('100%'),
                                width: '100%',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }} source={slide1} resizeMode='cover' /></View>
                    </Swiper>
                </View>
                 
                
            </>
        );
    }

}
export default TutScreen;