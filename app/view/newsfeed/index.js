import React from 'react';
import {View,Text,ScrollView,StyleSheet,Image,TextInput} from 'react-native';
import HomeScreenHeader from '../reusableComponents/internalScreenHeader';
import SliderImage1 from '../../approot/images/profile-img.png';
import Icon from 'react-native-vector-icons/FontAwesome';
import ScreenFooter from '../reusableComponents/internalScreenFooter';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class NewsfeedScreen extends React.Component{
    render(){
        return(
            <View style={{flex:1 , backgroundColor:'#fff'}}>
               <HomeScreenHeader title="Newsfeed" navigation={this.props.navigation}/>
                <ScrollView>
                <View style={{ flex: 1, backgroundColor: 'white' }}>

                    <View style={{ height: hp('10%'), width: wp('100%'), justifyContent: 'center', alignItems: 'center', borderBottomColor: 'grey', borderBottomWidth: 1 }}>
                    <View style={{ height: hp('6%'), width: wp('95%'), borderColor: 'grey', borderWidth: 1, borderRadius: hp('8%'), flexDirection: 'row' }}>
                        <View style={{ flex: 0.6, justifyContent: 'center' }}>
                        <TextInput 
                            underlineColorAndroid="transparent"
                            placeholder=" Write and post something..."
                            placeholderTextColor="#000"
                            autoCapitalize="none"
                        />
                        </View>
                        <View style={{ flex: 0.4, flexDirection: 'row' }}>
                            <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'flex-end' }}>
                                <Icon
                                name="file-image-o"
                                color='#D7A455'
                                size={hp('3%')}
                                />
                            </View>
                            <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center' }}>
                                <Icon
                                name="paperclip"
                                color='#D7A455'
                                size={hp('3%')}
                                />
                            </View>
                            <View style={{
                                flex: 0.4, height: hp('4%'), width: wp('15%'), backgroundColor: '#D7A455',
                                borderRadius: hp('8%'), flexDirection: 'row', justifyContent: 'center',
                                alignSelf: 'center', marginHorizontal: hp('0.4%')
                            }}>
                                <Text style={{ color: 'white', fontSize: hp('2%'), alignSelf: 'center' }}>
                                Post
                                </Text>
                            </View>
                        </View>
                    </View>
                    </View>

                <View style={[styles.raringReviewMainContainer, {}]}>
                    <View style={[styles.reviewCard, {}]}>

                        <View style={{ flex: 0.2 }}>
                            <View style={styles.dotView}>
                                <Image source={SliderImage1} style={{ height: '100%', width: '100%' }} />
                            </View>
                        </View>

                        <View style={{ flex: 0.8, flexDirection: 'column', justifyContent: 'center' }}>
                            <Text style={styles.nameStyle}>
                                Rosalyn Williams
                            </Text>
                            <View style={styles.timeMainView}>
                                <View style={styles.greyDotView}>
                                </View>
                                <Text style={styles.timeTextStyle}>
                                1 days ago
                                </Text>
                            </View>
                        </View>

                    </View>

                    <View style={{ flex: 1, flexDirection: 'row' }}>
                      <View style={{ flex: 0.12 }}>
                        </View>

                        <View style={[styles.textMainView, {}]}>
                            <View style={{ width: wp('90%'), }}>
                                <Text style={styles.ratingTextStyle}>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </Text>
                            </View>

                            <View style={{
                                height: hp('5%'), width: wp('90%'),
                                flexDirection: 'row',
                                justifyContent: 'space-between'
                            }}>

                                <View style={{ flexDirection: 'row', flex: 0.45, justifyContent: 'space-between' }}>
                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    <Icon
                                        name="thumbs-up"
                                        color='#D7A455'
                                        size={hp('2.3%')}
                                    />
                                    <Text style={{ color: 'grey', fontSize: hp('1.8%'), paddingHorizontal: hp('0.5%') }} >
                                        3 Likes
                                    </Text>
                                </View>

                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Icon
                                        name="thumbs-down"
                                        color='#D7A455'
                                        size={hp('2.3%')}
                                    />
                                    <Text style={{ color: 'grey', fontSize: hp('1.8%'), paddingHorizontal: hp('0.5%') }}  >
                                        3 Dislikes
                                    </Text>
                                </View>
                                </View>


                                <View style={{ flex: 0.55, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                }}>
                                    <Icon
                                        name="comment"
                                        color='#D7A455'
                                        size={hp('2.3%')}
                                    />
                                    <Text style={{ color: 'grey', fontSize: hp('1.8%'), paddingHorizontal: hp('0.6%') }}>
                                        20 Comments
                                    </Text>
                                </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    </View>
                    </View>
                </ScrollView>
                <ScreenFooter title="Terms & Conditions" navigation={this.props.navigation}/> 
            </View>
        )
    }
}


const styles = StyleSheet.create({
    timeTextStyle: {
       color: '#666',
       fontSize: hp('1.2%'),
       paddingHorizontal: hp('0.5%')
 
    },
    timeMainView: {
       flexDirection: 'row',
       paddingVertical: hp('0.5%')
    },
    textMainView: {
       flexDirection: 'column', flex: 0.88,
       
    },
    iconStyle: {
       flex: 0.05, paddingVertical: hp('0.3%')
    },
    raringReviewMainContainer: {
       // flex: 0.2,
     
       height: hp('26%'),
       borderBottomColor: 'grey', borderBottomWidth: 1,
       // borderColor:'red',borderWidth:1
    },
    reviewCard: {
       flex: 1,
       flexDirection: 'row',
       justifyContent: 'center',
       alignItems: 'center'
    },
    dotView: {
       height: hp('7%'),
       width: hp('7%'),
       borderRadius: hp('3.5%'),
       // backgroundColor: 'grey',
       marginHorizontal: hp('1.5%'),
       alignSelf: 'center'
    },
 
    greyDotView: {
       height: hp('1%'),
       width: hp('1%'),
       borderRadius: hp('1%'),
       backgroundColor: '#666',
       // marginHorizontal: hp('1.5%'),
       alignSelf: 'center'
    },
    ratingTextStyle: {
       color: '#666', fontSize: hp('1.8%'),
       paddingHorizontal: hp('1%')
    },
    nameStyle: {
       fontFamily: 'OpenSans-SemiBold',
       color: '#333',
       fontSize: hp('1.6%')
    }
 })
 