import React from 'react';
import {ImageBackground, Image, View,TouchableOpacity, TextInput, Text, ScrollView} from 'react-native';
import headImage from '../../approot/images/porfile-bg.png';
import ProfImg from '../../approot/images/profile-img.png';
import CustomStyle from '../../approot/stylesheet/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import HomeScreenHeader from '../reusableComponents/internalScreenHeader';
import ScreenFooter from '../reusableComponents/internalScreenFooter';
import { connect } from 'react-redux';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

class ProfileScreen extends React.Component 
{
     
  render() {
   var radio_props = [
      {label: 'Male    ', value: 1 },
      {label: 'Female', value: 2 }
    ];
    const source = this.props.user.userData && this.props.user.userData.profile_img !== null
        ? { uri: this.props.user.userData.img_url }
        : ProfImg;
     return (
      <>
      <HomeScreenHeader title="Profile" navigation={this.props.navigation}/>
      <ScrollView> 
      <ImageBackground
      style={CustomStyle.ProfileImageContainer}
        imageStyle={{}}
       source={headImage}>
          <Image source={source} style={CustomStyle.ProfileImageStyle}/>
          <TouchableOpacity
              style = {[CustomStyle.profileEditButton, CustomStyle.floatCenter]}
              onPress = {()=>this.props.navigation.navigate('EditProfile')}
              >
                
                <Text style = {[CustomStyle.profileEditButtonText, CustomStyle.floatCenter]} >
                   Edit  <Icon style={CustomStyle.IconStyle} name='pencil' color='#fff' size={16} />
                </Text>
                
           </TouchableOpacity>
       </ImageBackground>
         <View style = {CustomStyle.profilecontainer}>
          
         <TextInput style = {CustomStyle.input}
              underlineColorAndroid = "transparent"
              placeholder = "Full Name"
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              editable = {false}
              value={this.props.user.userData.first_name+' '+this.props.user.userData.last_name}
         />
           
         <TextInput style = {CustomStyle.input}
              underlineColorAndroid = "transparent"
              placeholder = "Email ID"
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              editable = {false}
              value={this.props.user.userData.email}
              />

           <TextInput style = {CustomStyle.input}
              underlineColorAndroid = "transparent"
              placeholder = "Mobile No."
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              editable = {false}
              value={this.props.user.userData.phone}
              /> 
            <Text style = {CustomStyle.Proflabeltext}>Gender</Text>
               <RadioForm
                  formHorizontal={true}
                  buttonColor={'#D2292B'}
                  animation={true}
                  radio_props={radio_props}
                  initial={3}
                  buttonSize={10}
                  buttonOuterSize={20}
                  style={{margin: '3%'}}
                  labelStyle={{fontSize: 16, color: '#D2292B'}}
                  onPress={(value) => {this.setState({value:value})}}
                  />
           
           
            <View style ={CustomStyle.profilebottomView}>
              <Text style = {[CustomStyle.linktextStyle, CustomStyle.floatCenter]}>Manage Addressses</Text> 
              <Text onPress = {()=>this.props.navigation.navigate('ChangepasswordOne')} style = {[CustomStyle.linktextStyle, CustomStyle.floatCenter]}>Change Password</Text>
           </View>

            
          
        </View>
       </ScrollView> 
       <ScreenFooter navigation={this.props.navigation}/>  
       </>

    );
  }
}

const mapStateToProps = state => ({
   user: state.UserReducer.user,
 });
export default connect(mapStateToProps)(ProfileScreen);
