import React from 'react';
import {ImageBackground, Image, View,TouchableOpacity, TextInput, Text, ScrollView} from 'react-native';
import headImage from '../../approot/images/fogot-password.jpg';
import backImage from '../../approot/images/back-arrow.png';
import CustomStyle from '../../approot/stylesheet/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import CheckBox from '@react-native-community/checkbox';


class SignupScreen extends React.Component 
{
  
  render() {
     return (
      <ScrollView> 
      <ImageBackground
      style={CustomStyle.ImageContainer}
        imageStyle={CustomStyle.ImageStyle}
       source={headImage}>
         <View style={CustomStyle.headcontainer}>

            <TouchableOpacity
               style = {CustomStyle.backbutton}
               onPress = {()=>this.props.navigation.goBack()}
            >
                  <Image source = {backImage} style={CustomStyle.imgStyle} />
            </TouchableOpacity>
            
            <View style = {CustomStyle.titletext}>
               <Text style={CustomStyle.headerTitleStyle}>
                 Reset Password
               </Text>
            </View>
            <View style = {CustomStyle.backbutton}></View>   
         </View>
         
         <View style = {CustomStyle.forgetcontainer}>

            <Text style = {[ CustomStyle.floatCenter,{fontSize:22,color: '#26024D',}]}>
                     OTP sent to below Mobile/Email Id 
            </Text>
            <Text style = {[ CustomStyle.floatCenter,{fontSize:20,color: '#D2292B',}]}>
                     dummymail@gmail.com {"\n"}
            </Text>
            <TouchableOpacity
              style = {[CustomStyle.submitButton, CustomStyle.floatCenter]}
              onPress = {()=>this.props.navigation.goBack()}>
                <Text style = {[CustomStyle.submitButtonText, CustomStyle.floatCenter]}>
                     Change & Resend 
                </Text>
            </TouchableOpacity>
          
            <TextInput style = {CustomStyle.input}
              underlineColorAndroid = "transparent"
              placeholder = "Enter OTP"
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              onChangeText = {this.handleName}
              />
            <Text style = {[ CustomStyle.floatRight,{fontSize:15, paddingRight:'3.5%', color: '#D2292B',textDecorationLine: "underline",}]}>
                     Resend OTP?
            </Text>
            <View style={CustomStyle.passwordContainer}>
               <TextInput
                  style={CustomStyle.inputStyle}
                     autoCorrect={false}
                     secureTextEntry = {true}
                     placeholderTextColor = "#000"
                     placeholder="Enter New Password"
                     onChangeText={this.onPasswordEntry}
                  />
               <Icon
                  style={CustomStyle.IconStyle}
                  name='eye'
                  color='#000'
                  size={20}
               />
            </View>
           
              
  
           <TouchableOpacity
              style = {[CustomStyle.submitButton, CustomStyle.floatCenter]}
              onPress = {()=>this.props.navigation.navigate('ChangepasswordOne')}
            >
                <Text style = {[CustomStyle.submitButtonText, CustomStyle.floatCenter]}> Confirm Now </Text>
            </TouchableOpacity>
           
          
        </View>
       </ImageBackground>
       </ScrollView> 

    );
  }
}

export default SignupScreen;