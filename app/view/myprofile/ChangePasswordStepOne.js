import React from 'react';
import {ImageBackground, Image, View,TouchableOpacity, TextInput, Text, ScrollView} from 'react-native';
import headImage from '../../approot/images/fogot-password.jpg';
import backImage from '../../approot/images/back-arrow.png';
import CustomStyle from '../../approot/stylesheet/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import CheckBox from '@react-native-community/checkbox';


class SignupScreen extends React.Component 
{
  
  render() {
     return (
      <ScrollView> 
      <ImageBackground
      style={CustomStyle.ImageContainer}
        imageStyle={CustomStyle.ImageStyle}
       source={headImage}>
         <View style={CustomStyle.headcontainer}>

            <TouchableOpacity
               style = {CustomStyle.backbutton}
               onPress = {()=>this.props.navigation.goBack()}
            >
                  <Image source = {backImage} style={CustomStyle.imgStyle} />
            </TouchableOpacity>
            
            <View style = {CustomStyle.titletext}>
               <Text style={CustomStyle.headerTitleStyle}>
                 Change Password
               </Text>
            </View>
            <View style = {CustomStyle.backbutton}></View>   
         </View>
         
         <View style = {CustomStyle.logincontainer}>
          
            <TextInput style = {CustomStyle.input}
              underlineColorAndroid = "transparent"
              placeholder = "Enter your Email ID/Mobile No."
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              onChangeText = {this.handleName}
              />
           
              
  
           <TouchableOpacity
              style = {[CustomStyle.submitButton, CustomStyle.floatCenter]}
              onPress = {()=>this.props.navigation.navigate('ChangepasswordTwo')}
            >
                <Text style = {[CustomStyle.submitButtonText, CustomStyle.floatCenter]}> Send OTP </Text>
            </TouchableOpacity>
           
          
        </View>
       </ImageBackground>
       </ScrollView> 

    );
  }
}

export default SignupScreen;