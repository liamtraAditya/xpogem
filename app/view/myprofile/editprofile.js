import React from 'react';
import {ImageBackground, Image, View,TouchableOpacity, TextInput, Text, ScrollView} from 'react-native';
import headImage from '../../approot/images/porfile-bg.png';
import ProfImg from '../../approot/images/profile-img.png';
import CustomStyle from '../../approot/stylesheet/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import HomeScreenHeader from '../reusableComponents/internalScreenHeader';
import RadioForm from 'react-native-simple-radio-button';
import { connect } from 'react-redux';
import * as ImagePicker from 'react-native-image-picker';

class EditProfileScreen extends React.Component 
{
   constructor(props){
      super(props);
      this.state={
         photo: null,
      }
   }  

   _handleSelectImage=()=>{
      ImagePicker.launchCamera(
         {
           maxHeight: 150,
           maxWidth: 150,
           includeBase64: false,
           mediaType: 'photo',
         },
         (response) => {
           
            if (response.didCancel) {
              
             } else {
               
               this._updateProfilePic(response);
                              }
      }
       )
     }

     _updateProfilePic=(response)=>{
      console.log("img-------launch image library----",response);
      // ImagePicker.launchImageLibrary(
      //    {
      //      mediaType: 'photo',
      //      includeBase64: false,
      //      maxHeight: 150,
      //      maxWidth: 150,
      //    },
      //    (response) => {
      //      console.log(response);
      //    },
      //  )
       let data = new FormData();
       console.log("data----after making object---",data);
       let obj = {
         uri: response.uri,
         name: response.fileName,
         type: response.type,
       };
       data.append('image', obj);
       console.log('image upload=dataaaa=>>>', data);
   
     }
   

  render() {
   //console.log('test',this.props);
   let radio_props = [
      {label: 'Male    ', value: 1 },
      {label: 'Female', value: 2 }
    ];
    const source = this.props.user.userData && this.props.user.userData.profile_img !== null
        ? { uri: this.props.user.userData.img_url }
        : ProfImg;
     return (
      <>
      <HomeScreenHeader title="Edit Profile" navigation={this.props.navigation}/>
      <ScrollView> 
      <ImageBackground
      style={CustomStyle.ProfileImageContainer}
        imageStyle={{}}
       source={headImage}>
          <Image source={source} style={CustomStyle.ProfileImageStyle}/>
          <TouchableOpacity
              style = {[CustomStyle.profileEditButton, CustomStyle.floatCenter]}
              onPress = {
                 () => this._handleSelectImage()
              }>
                
                <Text style = {[CustomStyle.profileEditButtonText, CustomStyle.floatCenter]}>
                   <Icon style={CustomStyle.IconStyle} name='camera' color='#fff' size={25} />
                </Text>
                
           </TouchableOpacity>
       </ImageBackground>
         <View style = {CustomStyle.profilecontainer}>
          
         <TextInput style = {CustomStyle.input}
              underlineColorAndroid = "transparent"
              placeholder = "First Name"
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              editable = {true}
              value={this.props.user.userData.first_name}
         />

         <TextInput style = {CustomStyle.input}
              underlineColorAndroid = "transparent"
              placeholder = "Last Name"
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              editable = {true}
              value={this.props.user.userData.last_name}
         />
           
         <TextInput style = {CustomStyle.input}
              underlineColorAndroid = "transparent"
              placeholder = "Email ID"
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              editable = {true}
              value={this.props.user.userData.email}
              />
 
           <TextInput style = {CustomStyle.input}
              underlineColorAndroid = "transparent"
              placeholder = "Mobile No."
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              editable = {true}
              value={this.props.user.userData.phone}
              /> 

            
               <Text style = {CustomStyle.Proflabeltext}>Gender</Text>
               <RadioForm
                  formHorizontal={true}
                  buttonColor={'#D2292B'}
                  animation={true}
                  radio_props={radio_props}
                  initial={this.props.user.userData.gender}
                  buttonSize={10}
                  buttonOuterSize={20}
                  style={{margin: '3%'}}
                  labelStyle={{fontSize: 16, color: '#D2292B'}}
                  onPress={(value) => {this.setState({value:value})}}
                  />
           
           
           <TouchableOpacity
              style = {[CustomStyle.submitButton, CustomStyle.floatCenter]}
              onPress = {
                 () => this.login(this.state.email, this.state.password)
              }>
                
                <Text style = {[CustomStyle.submitButtonText, CustomStyle.floatCenter]}> Save Changes </Text>
                
           </TouchableOpacity>

            
          
        </View>
       </ScrollView> 
       
       </>

    );
  }
}

const mapStateToProps = state => ({
   user: state.UserReducer.user,
 });
export default connect(mapStateToProps)(EditProfileScreen);
