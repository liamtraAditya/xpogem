import React from 'react';
import {ImageBackground, Image, View,TouchableOpacity,FlatList, TextInput, Text, ScrollView} from 'react-native';
import HomeScreenHeader from '../reusableComponents/homeScreenHeader';
import ScreenFooter from '../reusableComponents/internalScreenFooter';
import SliderCard from '../reusableComponents/DesignsliderCard';

class DesignScreens extends React.Component 
{
     
  render() {
//    console.log('test',this.props);
     return (
      <>
        <HomeScreenHeader title="Designs" navigation={this.props.navigation}/>
            <ScrollView style={{backgroundColor:'#fff'}}> 
             <View style={{flex:1,justifyContent:'center',marginTop:'2%', marginLeft:'2.5%'}} >
                <SliderCard searchTerm="Best Selling Products" featured='0' colNumber='2' horizontalView={false} navigation={this.props.navigation}/>
             </View>
            </ScrollView> 
        <ScreenFooter navigation={this.props.navigation}/>  
       </>

    );
  }
}
export default DesignScreens;
