import React from 'react';
import {Image, View, Text, ScrollView} from 'react-native';
import Style from '../../approot/stylesheet/custom';
import HomeScreenHeader from '../reusableComponents/homeScreenHeader';
import ScreenFooter from '../reusableComponents/internalScreenFooter';
import vendorImage from '../../approot/images/product-item-2.jpg';
import SliderComponent from '../reusableComponents/slider'
import SliderImage1 from '../../approot/images/banner-home-1.png';
import SliderImage2 from '../../approot/images/banner-home-2.png';
import SliderImage3 from '../../approot/images/sliderImage.png';
import SliderImage4 from '../../approot/images/sliderImage2.jpeg';
import FontAwsome from 'react-native-vector-icons/FontAwesome';
const sliderArray = [SliderImage1, SliderImage2, SliderImage3, SliderImage4]

class DesignDetailScreen extends React.Component 
{
     
  render() {

     return (
      <>
        <HomeScreenHeader title="Design Detail" navigation={this.props.navigation}/>
            <ScrollView>
                <SliderComponent slideImage={sliderArray} /> 
                <View style = {Style.orderContainer}>
                    <View style={Style.orderDetailSingleBox}>
                        <View style={[Style.rowdrection]}>
                            <View style={Style.orderRightcontent}>
                                <Text style={Style.headingtext} onPress={() => this.props.navigation.navigate('OrderDetail')}>Design Name Here..</Text>
                                <View style={[Style.rowdrection]}>
                                    <Text style={Style.sideHead}>Category :- </Text>
                                    <Text style={Style.paraText}>GEM STONE</Text>
                                </View>
                                <View style={[Style.rowdrection, Style.mb3]}>
                                    <Text style={[Style.sideHead, Style.width100]}>Design ID :  </Text>
                                    <Text style={[Style.paraText]}>JS665F8e97{'\n'}</Text>
                                </View> 
                                <View style={[Style.rowdrection]}>
                                    <FontAwsome name="star" color='#DBA153' size={25}/>
                                    <Text style={{ fontSize:16,color:'#999',marginLeft:10}}>3.4{'\n'}</Text>
                                </View>
                            
                            </View>
                            <View style={[Style.orderContent]}>
                                <FontAwsome name="heart-o" color='#DBA153' size={25} style={{alignSelf:'flex-end'}}/>
                            </View>
                        </View>
                        
                                          
                    </View>
                    <View style={Style.orderDetailSingleBox}>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.sideHead]}>Design Description :  </Text>
                        </View>   
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.paraText]}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod b
                            ibendum laoreet. Proin gravida dolor sit amet lacus ac cumsan et viverra ju
                            sto commodo
                            </Text>
                        </View>
                        <Text style={{color: '#D2292B',textDecorationLine: "underline", fontSize:12, alignSelf:'flex-end',}}>
                            Read more...
                        </Text>
                    </View>
                    <View style={Style.orderSingleBox}>
                        <View style={Style.orderContent}>
                            <Image source={vendorImage} style={Style.orderImage}></Image>
                        </View>
                        <View style={Style.orderRightcontent}>
                            <Text style={Style.headingtext}>Material Used</Text>
                            {/* <Text style={[Style.paraText, Style.mb3]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text> */}
                            <View>
                                <Text style={[Style.sideHead]}>Yellow horn rondelle bead, 6-8mm</Text>
                                <Text style={[Style.sideHead]}>Yellow horn rondelle bead, 6-8mm</Text>
                                <Text style={[Style.sideHead]}>Yellow horn rondelle bead, 6-8mm</Text>
                                <Text style={[Style.sideHead]}>Yellow horn rondelle bead, 6-8mm</Text>
                                <Text style={[Style.sideHead]}>Yellow horn rondelle bead, 6-8mm</Text>
                                <Text style={[Style.sideHead]}>Yellow horn rondelle bead, 6-8mm</Text>
                            </View>
                            <Text style={{color: '#D2292B',marginTop:'7%', textDecorationLine: "underline",}}>Add all items to cart</Text>
                        </View>
                        
                    </View>
                </View>
            </ScrollView> 
       <ScreenFooter navigation={this.props.navigation}/>  
       </>

    );
  }
}
export default DesignDetailScreen;
