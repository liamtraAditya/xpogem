import React from 'react';
import { ImageBackground, Image, View, TouchableOpacity, TextInput, Text, ScrollView, FlatList } from 'react-native';
import CustomStyle from '../../approot/stylesheet/index';
import HomeScreenHeader from '../reusableComponents/homeScreenHeader';
import rightIcon from '../../approot/images/icons/left-anle.png'
import notificationIcon from '../../approot/images/icons/notification-button.png'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';



class SettingScreen extends React.Component {

    render() { //console.log('props drawer comasdsaponent--------->>>>>', this.props.navigation);
        return (
            <>
                <HomeScreenHeader title="Setting" navigation={this.props.navigation}/>
                <ScrollView>
                    <View style={CustomStyle.SettinglinkContainer}>
                        <Text style={CustomStyle.settingText}>Notification</Text>
                        <Image style={CustomStyle.settingNotificationIcon} source={notificationIcon}/>
                    </View>
                    <TouchableOpacity style={CustomStyle.SettinglinkContainer} onPress = {() => this.props.navigation.navigate('Privacy') }>
                        <Text style={CustomStyle.settingText}>Privacy Policy</Text>
                        <Image style={CustomStyle.settingIcon} source={rightIcon}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={CustomStyle.SettinglinkContainer} onPress = {() => this.props.navigation.navigate('Terms') }>
                        <Text style={CustomStyle.settingText}>Terms & Conditions</Text>
                        <Image style={CustomStyle.settingIcon} source={rightIcon}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={CustomStyle.SettinglinkContainer} onPress = {() => this.props.navigation.navigate('Faq') }>
                        <Text style={CustomStyle.settingText}>FAQ's</Text>
                        <Image style={CustomStyle.settingIcon} source={rightIcon}/>
                    </TouchableOpacity>
                    
                </ScrollView>
                
            </>
        );
    }

}
export default SettingScreen;