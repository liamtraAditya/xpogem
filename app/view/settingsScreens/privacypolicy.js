import React from 'react';
import { ImageBackground, Image, View, TouchableOpacity, TextInput, Text, ScrollView, FlatList } from 'react-native';
import CustomStyle from '../../approot/stylesheet/index';
import HomeScreenHeader from '../reusableComponents/internalScreenHeader';
import ScreenFooter from '../reusableComponents/internalScreenFooter';

class PrivacyScreen extends React.Component {

    render() {
        return (
            <>
                <HomeScreenHeader title="Privacy Policy" navigation={this.props.navigation}/>
                <ScrollView style={{backgroundColor:'#fff',}}>
                    
                    
                        <Text style={{margin:'4%',fontSize:20,justifyContent:'center'}}>
                           {'\n'}
                           It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                           {'\n\n'}
                           It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                        </Text>
                </ScrollView>
                <ScreenFooter title="Terms & Conditions" navigation={this.props.navigation}/>  
            </>
        );
    }

}
export default PrivacyScreen;