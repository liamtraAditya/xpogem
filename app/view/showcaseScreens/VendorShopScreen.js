import React from 'react';
import {Image, View, Text, ScrollView,FlatList} from 'react-native';
import Style from '../../approot/stylesheet/custom';
import HomeScreenHeader from '../reusableComponents/homeScreenHeader';
import ScreenFooter from '../reusableComponents/internalScreenFooter';
import vendorImage from '../../approot/images/vendor-img.png';
import FontAwsome from 'react-native-vector-icons/FontAwesome';
import ProductSliderCard from '../reusableComponents/ProductsliderCard';
import CustomStyle from '../../approot/stylesheet/index';
import orderImage from '../../approot/images/order.png';

class DesignDetailScreen extends React.Component 
{
     
  render() {

     return (
      <>
        <HomeScreenHeader title="Shop Detail" navigation={this.props.navigation}/>
            <ScrollView>
                <View style = {Style.orderContainer}>
                    <View style={Style.orderDetailSingleBox}>
                    <View style={Style.orderSingleBox}>
                        <View style={Style.orderContent}>
                            <Image source={vendorImage} style={Style.orderImage}></Image>
                        </View>
                        <View style={Style.orderRightShopcontent}>
                            <Text style={Style.headingtext} onPress={() => this.props.navigation.navigate('OrderDetail')}>Vendor Name Here..</Text>
                            {/* <Text style={[Style.paraText, Style.mb3]}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text> */}
                            <View style={[Style.rowdrection]}>
                                <Text style={[Style.sideHead]}>Store Name :- </Text>
                                <Text style={[Style.paraText]}>GEM STONE{'\n'}</Text>
                            </View>
                            <View style={[Style.rowdrection]}>
                                    <FontAwsome name="star" color='#DBA153' size={25}/>
                                    <Text style={{ fontSize:16,color:'#999',marginLeft:10}}>3.4{'\n'}</Text>
                             </View>
                             
                        </View>
                        <View style={[Style.orderContent]}>
                                <FontAwsome name="heart-o" color='#DBA153' size={25} style={{alignSelf:'flex-end'}}/>
                        </View>
                        
                    </View>
                        
                                          
                    </View>
                    <View style={Style.orderDetailSingleBox}>
                        <View style={CustomStyle.slideCardHeader}>
                                <Text style={CustomStyle.slideCardTitleStyle}>Vendor Description</Text>
                        </View>
                        <View style={[Style.rowdrection, Style.mb3]}>
                            <Text style={[Style.paraText]}>
                            {'\n'}Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod b
                            ibendum laoreet. Proin gravida dolor sit amet lacus ac cumsan et viverra ju
                            sto commodo
                            </Text>
                        </View>
                        <Text style={{color: '#D2292B',textDecorationLine: "underline", fontSize:12, alignSelf:'flex-end',}}>
                            Read more...
                        </Text>
                    </View>
                    <View style={Style.orderDetailSingleBox}>
                        <View style={CustomStyle.slideCardHeader}>
                                <Text style={CustomStyle.slideCardTitleStyle}>Shop Images</Text>
                        </View>
                        <View>
                        <FlatList style={{backgroundColor:'#fff'}} showsHorizontalScrollIndicator={false} horizontal={true} data="hello00" renderItem={() => {
                            return  <View style={Style.orderSliderBox}>
                                        <Image style={Style.orderSliderBox} source={orderImage}></Image>
                                    </View>
                            }} />
                        </View>
                    </View>
                    <View style={CustomStyle.flexOne}>
                        <View style={CustomStyle.slideCardContainer}>
                            <View style={CustomStyle.slideCardHeader}>
                                <Text style={CustomStyle.slideCardTitleStyle}>Vendor Products</Text>
                            </View>
                            <Text style={CustomStyle.slideCardViewAllTextStyle} onPress = {() => this.props.navigation.navigate('ProductList')}>View all</Text>
                        </View>
                        <ProductSliderCard colNumber='0' searchTerm="Best Selling Products" featured='0' horizontalView={true} navigation={this.props.navigation}/>
                    </View>
                </View>
            </ScrollView> 
       <ScreenFooter navigation={this.props.navigation}/>  
       </>

    );
  }
}
export default DesignDetailScreen;
