import React from 'react';
import { ImageBackground, Image, View, TouchableOpacity, TextInput, Text, ScrollView, FlatList } from 'react-native';
import CustomStyle from '../../approot/stylesheet/index';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SliderComponent from '../reusableComponents/slider'
import SliderImage1 from '../../approot/images/banner-home-1.png';
import SliderImage2 from '../../approot/images/banner-home-2.png';
import SliderImage3 from '../../approot/images/sliderImage.png';
import SliderImage4 from '../../approot/images/sliderImage2.jpeg';
import CategoryButton from '../reusableComponents/categoryButton';
import ProductSliderCard from '../reusableComponents/ProductsliderCard';
import DesignSliderCard from '../reusableComponents/DesignsliderCard';
import HomeScreenHeader from '../reusableComponents/homeScreenHeader';
const sliderArray = [SliderImage1, SliderImage2, SliderImage3, SliderImage4]
class HomeScreen extends React.Component {

    render() {
        return (
            <>
                <HomeScreenHeader title="XpoGem" navigation={this.props.navigation}/>
                <ScrollView>
                    <SliderComponent slideImage={sliderArray} />
                    {/* Category */}
                    <View style={[CustomStyle.flexOne, CustomStyle.addShadow, { marginTop: hp('0.5%'), backgroundColor: '#fff' }]}>
                        <View style={CustomStyle.categoryListContainer}>

                            <CategoryButton name="Stone Decor" image={SliderImage3} navigation={this.props.navigation}/>
                            <CategoryButton name="Rocks" image={SliderImage4} navigation={this.props.navigation}/>
                            <CategoryButton name="Minirals" image={SliderImage3} navigation={this.props.navigation}/>
                            <CategoryButton name="Loose Gems" image={SliderImage4} navigation={this.props.navigation}/>

                        </View>
                        <View style={CustomStyle.categoryListContainer}>
                            <CategoryButton name="Jwellery" image={SliderImage3} navigation={this.props.navigation}/>
                            <CategoryButton name="Minirals" image={SliderImage4} navigation={this.props.navigation}/>
                            <CategoryButton name="GEMs" image={SliderImage3} navigation={this.props.navigation}/>
                            <CategoryButton name="Loose" image={SliderImage4} navigation={this.props.navigation}/>
                        </View>
                    </View>
                    <View style={CustomStyle.bannerContainer}>
                        <Image source={SliderImage2} style={{ height: '100%', width: '100%' }} />
                    </View>

                    <View style={CustomStyle.flexOne}>
                        <View style={CustomStyle.slideCardContainer}>
                            <View style={CustomStyle.slideCardHeader}>
                                <Text style={CustomStyle.slideCardTitleStyle}>Hot Selling Products</Text>
                            </View>
                            <Text style={CustomStyle.slideCardViewAllTextStyle} onPress = {() => this.props.navigation.navigate('ProductList')}>View all</Text>
                        </View>
                        <ProductSliderCard colNumber='0' searchTerm={[{Featured:1}]} horizontalView={true} navigation={this.props.navigation}/>
                    </View>

                    <View style={CustomStyle.bannerContainer}>
                        <Image source={SliderImage1} style={{ height: '100%', width: '100%' }} />
                    </View>
                    <View style={CustomStyle.flexOne}>
                        <View style={CustomStyle.slideCardContainer}>
                            <View style={CustomStyle.slideCardHeader}>
                                <Text style={CustomStyle.slideCardTitleStyle}>Top Designs</Text>
                            </View>
                            <Text style={CustomStyle.slideCardViewAllTextStyle} onPress = {() => this.props.navigation.navigate('DesignList')}>View all</Text>
                        </View>
                        <DesignSliderCard colNumber='0' searchTerm="Best Selling Products" featured='0' horizontalView={true} navigation={this.props.navigation}/>
                    </View>
                </ScrollView>
                
            </>
        );
    }

}
export default HomeScreen;