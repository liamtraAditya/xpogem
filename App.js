
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React from 'react';
import {StatusBar} from 'react-native';
import Appcontr from './app/config/navigation/index';
import 'react-native-gesture-handler';
import {Provider} from 'react-redux';
import store from './app/config/redux/store/index';

class App extends React.Component {
  render() {
    return (

      <Provider style={{flex: 1}} store={store}>
        <StatusBar backgroundColor="#26024D" />
        <Appcontr />
      </Provider>

    );
  }
}

export default App;